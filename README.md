
![Logo](https://e1.pxfuel.com/desktop-wallpaper/309/536/desktop-wallpaper-data-science-data-structure.jpg)


# Data Analysis and Data Science Projects In Python

In this repository, exploratory analysis of data from different sources are critically performed.


## Author

- [@eldrigeampong](https://www.github.com/eldrigeampong)


## Usage/Examples

```python
import pandas as pd
import tensorflow as tf

print(pd.__version__)
print(tf.__version__)
```