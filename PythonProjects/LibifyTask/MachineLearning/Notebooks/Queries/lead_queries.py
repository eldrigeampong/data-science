query1 = """
         -- Returns an Overview Of All Leads -- reporting-db.libify.cloud:5432  (Updated 14.02.2024)

with
    leads_tbl as (
                    select 
                          distinct 
                                  osl.lead_id,
				                  osl.first_name as lead_first_name,
				                  osl.last_name as lead_last_name,	
					              -- adjust affiliate code just as in View leads_subscription
				                  coalesce(osl.affiliate_code, 'LIBIFYO'::character varying(255)) as aff_code,
				                  osl.createdat as lead_creat_dt,
				                  osl.status_id as lead_status,
					              -- adjust reason for better readability
				                  replace(osl.reason, '$', '') as lead_reason,
				                  -- build call center groups
				                  (
				                    case
					                    when lower(oso.cc_company_name) like 'comcare%' then 'Comcare'
					                    when (lower(oso.cc_company_name) like '%covendos%') or (oso.cc_company_name in ('4058', '4154', '4325', '4346', '4368')) then 'Covendos'
					                    when lower(oso.cc_company_name) like 'km%' then 'KM Co'
					                    when lower(oso.cc_company_name) like 'libify%' then 'Libify'
					                    when oso.cc_company_name in ('Philipp.kollmannsberger', 'YagoErice') then 'Libify Owned and Operated/PM'
					                    when lower(oso.cc_company_name) like 'telepower%' then 'Telepower'
					                    when lower(oso.cc_company_name) like '%epfortuna%' then 'Epfortuna'
					                    when lower(oso.cc_company_name) like 'ifk%' then 'lfk'
					                   else 'Sonstige'
					                end
					              ) as cc_group,
					              -------------------     			
				                  alg.name as lead_group,
				                  apm.name as price_model,
				                  apm.price,
				                  a.df_task,
				                  a.campaign_name as campaign
                    from oro_mysql.orocrm_sales_lead osl	
			        left join  oro_mysql.orocrm_sales_opportunity oso on osl.id = oso.lead_id	        -- the call center names are properly filled
			        left join affiliates a on a.affiliate_code = osl.affiliate_code			
			        left join affiliate_lead_groups alg on alg.id = a.lead_group_id
			        left join affiliate_affiliate_price_models aapm on a.id = aapm.affiliate_id 	    -- extra join needed to fetch all price_models
			        left join affiliate_price_models apm on apm.id = aapm.affiliate_price_model_id 	
                  ),
	monsum_tbl as (
	                 select
	  					   distinct
		      					   mc."CUSTOMER_ID" as cust_id,
	          					   ms."SUBSCRIPTION_ID" as subsc_id,
			                       ms."START" as subsc_start,
	                               -- adjust the cancellation date for errors
			                       (
			                         case
										 when ms."CANCELLATION_DATE" < ms."START" then ms."START"
		          					    else ms."CANCELLATION_DATE"
	                                 end
	                               ) as subsc_end,
	                               -- adjust the cancellation notificaton date: replace the nulls by the adjusted cancellation date (if notnull)
			                       ( 
			                         case 
					                     when ms."CANCELLATION_NOTIFICATION_DATE" isnull and ms."CANCELLATION_DATE" < ms."START" then ms."START"
		                                 when ms."CANCELLATION_NOTIFICATION_DATE" isnull and ms."CANCELLATION_DATE" >= ms."START" then ms."CANCELLATION_DATE"
		                               else ms."CANCELLATION_NOTIFICATION_DATE"
	                                 end
	                               ) as subsc_end_note,
			                       ms."STATUS" as subsc_status,
			                       ms."ARTICLE_NUMBER" as art_no, 
			                       -- adjust article names
                                   /*
                                      * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                                      * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                                   */
                                   (
                                     case
	                                     when ms."ARTICLE_NUMBER" = '0088' then 'HiB'
                                         when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                                         when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                                         when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                                         when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                                       else null
                                      end
	                               ) as prod,
			                       ms."X_ATTRIBUTES_LEAD" as ms_lead_id
                     from oro_mysql.monsum__subscription ms
                     left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
                     /*
                        * exclude all cases where a customer has no id
                        * include Basic, Mobil, Home and Doro products
                        * HiB is only included here as service not a product
                     */
                     where mc."CUSTOMER_ID" notnull and 
                     ms."ARTICLE_NUMBER" in ('0088', '1900', '1901', '1902', '1904', '1905', '1906')
	              ),
	lead_monsum_tbl as ( -- Merge LEAD + MONSUM Tables
	                      select
	                            distinct  
	                                    ----------------- lead info
				                        lead_id,
				                        lead_first_name,
				                        lead_last_name,	
				                        aff_code,
				                        --lead_creat_dt,
					                    -- !!!!!!!! correct for ERRORS IN DATA (leads created after they've already purchased a subscription)
				                        (
				                           case 		
					                           when lead_creat_dt > subsc_start then subsc_start
					                          else lead_creat_dt
				                           end
				                        ) as lead_creat,
				                        lead_status,
				                        lead_reason,
				                        cc_group,
				                        lead_group,
				                        ----------------- customer info
				                        cust_id,
				                        ----------------- subscription info
				                        subsc_id,
				                        subsc_start,
				                        subsc_end,
				                        subsc_end_note,
					                    -- calculate the length in days of a subscription
				                        (
				                          (
				                             case 
											     when subsc_end isnull then now()
					                           else subsc_end
				                             end
				                          )::date - subsc_start::date
				                        ) as subsc_len_d,
				                        (
				                          (
				                             case 
					                             when subsc_end_note isnull then now()
					                           else subsc_end_note
				                             end
				                          )::date - subsc_start::date
				                        ) as subsc_len_d_note,
				                        subsc_status,
				                        prod,
				                        ----------------- affiliate info
					                    -- calculate the price model type (CPL, CPO, etc.)
				                        (
				                           case
					                           when price_model like '%CPL' then 'CPL'
					                           when price_model like '%CPO' then 'CPO'
					                         else price_model
				                           end
				                        ) as price_model_type,
				                        price,
				                        df_task,
				                        campaign,
				                        ----------------- further technically facilitating variables				
				                        min(subsc_start) over (partition by cust_id) as cust_start_1st,
				                        max(subsc_end) over (partition by cust_id) as cust_end_last,
				                        max(subsc_end_note) over (partition by cust_id) as cust_end_last_note,
				                        min(subsc_start) over (partition by lead_id) as lead_subsc_start_1st,
				                        dense_rank() over (partition by cust_id, subsc_id) as subsc_per_cust_rk,
					                    -- chronological order of subscriptions per lead: subscriptions started and respectively ended by a lead
				                        row_number() over (partition by lead_id, subsc_id order by subsc_start) as rn_start,
	                                    --row_number() over (partition by lead_id, subsc_id order by subsc_end) as rn_end,	            
	                                    --row_number() over (partition by lead_id, subsc_id order by subsc_end) as rn_end_note,
	            	                    -- !!!!!!!!! adjust for LEAD DUPLICATES (leads with multiple creation dates) --> order by the adjusted creation date
	                                    row_number() over (partition by lead_id order by (
	                                                                                        case 		
							                                                                    when lead_creat_dt < subsc_start then subsc_start
							                                                                  else lead_creat_dt
						                                                                    end
						                                                                  )
						                ) as rn_lead,            
					                    -- order products per customer following start_date and (in case with multiple on same start_date also) end_date
				                        row_number() over (partition by cust_id order by subsc_start, subsc_end) as rn_prod,
				                        row_number() over (partition by cust_id order by subsc_start, subsc_end_note) as rn_prod_note,
				                        -----------------
				                        -- create date calendars for relationships in Power BI, s.t. the original dates are kept as datetime
				                        subsc_start::date as start_cal,	
				                        subsc_end::date as end_cal,					
				                        subsc_end_note::date as end_cal_note	
	                      from leads_tbl
	                      left join monsum_tbl on ms_lead_id = lead_id
	                   ),
	metrics_tbl_first as (-- Compute first half of metrics calculation
							select 
								  distinct 
			                              lead_id,
			                              lead_first_name,
			                              lead_last_name,
			                              (
			                                case 
												when lead_first_name like 'unknown%' and lead_last_name = lead_id then 1
				                              else 0
										    end
										  ) as is_unknown,			
										  aff_code,
			                              lead_creat,
			                              lead_status,
			                              lead_reason,
			                              cc_group,
			                              lead_group,
			                              price_model_type,
			                              price,
			                              df_task,
			                              campaign,
			                              subsc_id,
			                              subsc_start,
			                              subsc_end,
			                              subsc_end_note,
			                              subsc_status,
			                              subsc_len_d,			
			                              subsc_len_d_note,
				                          /*
				                           * calculate the type of subscription: 
				                           * active (i.e. not cancelled), 
				                           * cancellation (end date given, duration of under 60 days), 
				                           * churn (end date given, duration of at least 60 days)
				                           */
			                               (
			                                  case 
				                                  when subsc_start is not null and subsc_end isnull then 'active'
				                                  when subsc_end is not null and subsc_len_d < 60 then 'cancellation'
				                                  when subsc_end is not null and subsc_len_d >= 60 then 'churn'	
			                                  end
			                               ) as subsc_type,
			                               (
			                                  case 
				                                  when subsc_start is not null and subsc_end_note isnull then 'active'
				                                  when subsc_end_note is not null and subsc_len_d_note < 60 then 'cancellation'
				                                  when subsc_end_note is not null and subsc_len_d_note >= 60 then 'churn'	
			                                  end
			                               ) as subsc_type_note,
			                               cust_id,
				                           -- calculate the number of days customers are active
			                               (
			                                  (
			                                     case 
				                                     when cust_end_last isnull then now()
				                                   else cust_end_last
			                                     end
			                                  )::date - cust_start_1st::date
			                               ) as cust_active_d,
			                               (
			                                  (
			                                     case 
	                                                 when cust_end_last_note isnull then now()
				                                   else cust_end_last_note
			                                     end
			                                  )::date - cust_start_1st::date
			                               ) as cust_active_d_note,
			                               prod,
				                           -- adjust product order s.t. HiB is backed to the end of the product list, because HiB is not a product, but a service (!)
			                               (
			                                  case 
				                                  when prod = 'HiB' then 9999
				                                else rn_prod
			                                  end
			                               ) as rn_prod_adj,
			                               (
			                                  case 
				                                  when prod = 'HiB' then 9999
				                                else rn_prod_note
			                                  end
			                               ) as rn_prod_note_adj,
			                               start_cal,
			                               end_cal,
			                               end_cal_note,
			                               lead_creat::date as creat_cal,
				                           -- count acquisitions according to the subscription start calendar
			                               (
			                                 case 
				                                 when rn_start = 1 and subsc_start notnull then 1
				                               else 0
			                                 end
			                               ) as is_acqui,
				                           -- count leads according to the lead creation calendar
			                               (
			                                 case 
				                                 when rn_lead = 1 then 1
				                               else 0
			                                 end
			                               ) as is_creat
		                    from lead_monsum_tbl  
			                -- filter out possible subscription duplicates for each customer
		                    where subsc_per_cust_rk = 1	
		                 ),
	metrics_tbl_second as ( 
							select 
							      distinct 
			                              lead_id,
			                              lead_first_name,
			                              lead_last_name,
			                              is_unknown,			
			                              aff_code,
			                              lead_creat,
			                              lead_status,
			                              lead_reason,  
			                              cc_group,
			                              lead_group,
			                              price_model_type,
			                              price,
			                              df_task,
			                              is_creat,
			                              campaign,
			                              subsc_id,
			                              subsc_start,
			                              subsc_end,
			                              subsc_end_note,
			                              subsc_status,
			                              subsc_type,
			                              subsc_type_note,
			                              subsc_len_d,
			                              subsc_len_d_note,
			                              cust_id,
			                              cust_active_d,
			                              cust_active_d_note,
			                              prod,
			                              is_acqui,	
				                          -- compute the previous product (lag)
			                              (
			                                 case 
				                                 when prod = 'HiB' or (lag(prod) over (partition by cust_id order by rn_prod_adj) = 'HiB') then null
				                               else lag(prod) over (partition by cust_id order by rn_prod_adj)
			                                 end
			                              ) as previous_prod,
			                              (
			                                 case 
				                                 when prod = 'HiB' or (lag(prod) over (partition by cust_id order by rn_prod_note_adj) = 'HiB') then null
				                               else lag(prod) over (partition by cust_id order by rn_prod_note_adj)
			                                 end
			                              ) as previous_prod_note,						
				                          -- compute the next product (lead)
			                              (
			                                 case 
				                                 when prod = 'HiB' or (lead(prod) over (partition by cust_id order by rn_prod_adj) = 'HiB') then null
				                               else lead(prod) over (partition by cust_id order by rn_prod_adj)
			                                 end
			                              ) as next_prod,
			                              (
			                                case 
				                                when prod = 'HiB' or (lead(prod) over (partition by cust_id order by rn_prod_note_adj) = 'HiB') then null
				                              else lead(prod) over (partition by cust_id order by rn_prod_note_adj)
			                                end
			                              ) as next_prod_note,
			                              start_cal,
			                              end_cal,
			                              end_cal_note,
			                              creat_cal
			                from metrics_tbl_first
			              ),
	final_tbl as (
	                select
	                      distinct 
	                              lead_id,
			                      lead_first_name,
			                      lead_last_name,
			                      is_unknown,			
			                      aff_code,
			                      lead_creat::date,
			                      extract(year from lead_creat::date) as lead_creat_yr,
			                      extract(month from lead_creat::date) as lead_creat_mnth,
			                      to_char(lead_creat::date, 'Mon') as lead_creat_mnth_name,
			                      extract(day from lead_creat::date) as lead_creat_day,
			                      to_char(lead_creat::date, 'Dy') as lead_creat_day_name,
			                      lead_status,
			                      lead_reason,
			                      cc_group,
			                      lead_group,
			                      price_model_type,
			                      price,
			                      df_task,
			                      is_creat,
			                      campaign,
			                      cust_id,
			                      subsc_id,
			                      subsc_start::date,
			                      extract(year from subsc_start::date) as subsc_start_yr,
			                      extract(month from subsc_start::date) as subsc_start_mnth,
			                      to_char(subsc_start::date, 'Mon') as subsc_start_mnth_name,
			                      extract(day from subsc_start::date) as subsc_start_day,
			                      to_char(subsc_start::date, 'Dy') as subsc_start_day_name,
			                      subsc_end::date,
			                      subsc_end_note::date,
			                      subsc_status,
			                      subsc_type,
			                      subsc_type_note,
			                      subsc_len_d,
			                      subsc_len_d_note,
			                      cust_active_d,
			                      cust_active_d_note,
			                      prod,
			                      is_acqui,
			                      previous_prod,
			                      previous_prod_note,
			                      next_prod,
			                      next_prod_note,
				                  -- mark the PREVIOUS product as product change (change start is pc)
			                      (
			                         case 
				                         when prod != 'HiB' and prod != next_prod then 1 --(prod != next_prod or prod != previous_prod)  then 1
				                       else 0
			                         end
			                      ) as is_pc1,
			                      (
			                         case 
				                         when prod != 'HiB' and prod != next_prod_note then 1 --(prod != next_prod or prod != previous_prod)  then 1
				                       else 0
			                         end
			                      ) as is_pc1_note,
				                  -- mark the NEXT product as product change (change destination is pc)
			                      (
			                         case 
				                         when prod != 'HiB' and prod != previous_prod  then 1
				                       else 0
			                         end
			                      ) as is_pc2,	
			                      (
			                         case 
				                         when prod != 'HiB' and prod != previous_prod_note  then 1
				                       else 0
			                         end
			                      ) as is_pc2_note,
			                      start_cal,
			                      end_cal,
			                      end_cal_note,
			                      creat_cal
	                from metrics_tbl_second
	             )
-- select distinct * from leads_tbl 
-- select distinct * from monsum_tbl
-- select distinct * from lead_monsum_tbl 
-- select distinct * from metrics_tbl_first
-- select distinct * from metrics_tbl_second
select distinct * from final_tbl order by lead_id, subsc_start       

         """