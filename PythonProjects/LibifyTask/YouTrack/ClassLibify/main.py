import pandas as pd
from attrs import define
from credentials import *
from sqlalchemy.engine import URL
from sqlalchemy import text, create_engine


@define
class DataExtractor:
  pg_user: str = repo_db_user
  pg_pwd: str = repo_db_pwd
  pg_host: str = "reporting-db.libify.cloud"
  pg_db_name: str = "cr-reporting"
  pg_port = 5432

  rm_user: str = redmine_db_user
  rm_pwd: str = redmine_db_pwd
  rm_host: str = "192.168.150.194"
  rm_db_name: str = "redmine"
  rm_port = 5432

  oro_user: str = mysql_user
  oro_pwd: str = mysql_pwd
  oro_host: str = "oro-mysql-slave.libify.com"
  oro_db_name: str = "oro"
  oro_port = 30316

  def get_data_repo_db(self, sql_query: str) -> pd.DataFrame:
    """
    Extracts Data From PostgreSQL Database

    Parameters
    ----------
    sql_query: str or SQLAlchemy Selectable (select or text object)
      SQL query to be executed or a table name.

    Returns:
      Pandas DataFrame

    """
    url_object = URL.create("postgresql+psycopg2", username=self.pg_user, password=self.pg_pwd, host=self.pg_host, database=self.pg_db_name, port=self.pg_port)
    engine = create_engine(url_object)

    return pd.read_sql(sql=text(sql_query), con=engine)
  

  def get_data_redmine_db(self, sql_query: str) -> pd.DataFrame:
    """
    Extracts Data From PostgreSQL Database

    Parameters
    ----------
    sql_query: str or SQLAlchemy Selectable (select or text object)
      SQL query to be executed or a table name.

    Returns:
      Pandas DataFrame

    """
    url_object = URL.create("postgresql+psycopg2", username=self.rm_user, password=self.rm_pwd, host=self.rm_host, database=self.rm_db_name, port=self.rm_port)
    engine = create_engine(url_object)

    return pd.read_sql(sql=text(sql_query), con=engine)
  

  def get_data_oro_db(self, sql_query: str) -> pd.DataFrame:
    """
    Extracts Data From MySQL Database

    Parameters
    ----------
    sql_query: str or SQLAlchemy Selectable (select or text object)
      SQL query to be executed or a table name.

    Returns:
      Pandas DataFrame

    """
    url_object = URL.create("mysql+mysqldb", username=self.oro_user, password=self.oro_pwd, host=self.oro_host, database=self.oro_db_name, port=self.oro_port)
    engine = create_engine(url_object)

    return pd.read_sql(sql=text(sql_query), con=engine)