query1 = """
        -- Returns Libify Customers For All Products -- reporting-db.libify.cloud:5432  (Updated 23.01.2024)

with
	cte1 as (
	          select
	                distinct 
                            -- replace empty serial numbers with null
	                        replace(lo.items_serial_numbers, '[""]', null) as items_serial_numbers,
	                        -- extract device type
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then right(split_part(items_serial_numbers, '-', 3), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(left(split_part(items_serial_numbers, ',', 2), 4), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then substring(split_part(items_serial_numbers, ',', 2), 2, 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
	               		    	when items_serial_numbers like '%GC%' then trim(split_part(items_serial_numbers, '-', 1), '["')
	               		    	when items_serial_numbers like '%SM%' then right(split_part(items_serial_numbers, '-', 1), 2)
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then right(split_part(items_serial_numbers, '-', 1), 2) 
	               		    	when items_serial_numbers like '%SN%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%LS%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%TRC%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '["RB%'then right(split_part(items_serial_numbers, '-', 1), 3) 
	               		    	when items_serial_numbers like '["RB%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '%JY%' then substring(items_serial_numbers, 3, 2)
	               		    end
	               		   ) as device_type,
	               		   -- extract serial numbers
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')  
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then trim(substring(items_serial_numbers, 10, 11), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%GC%' then trim(substring(items_serial_numbers, 3, 10), '"\][,')
	               		    	when items_serial_numbers like '%SM%' then trim(substring(items_serial_numbers, 3, 11), '"\,')
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%SN%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%LS%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    	when items_serial_numbers like '%TRC%' then trim(substring(items_serial_numbers, 3, 14), '"\][,')
	               		    	when items_serial_numbers like '["RB%' then trim(substring(items_serial_numbers, 3, 12), '"\][,')
	               		    	when items_serial_numbers like '%JY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    end
	               		    ) as serial_number,
	                        mc."CUSTOMER_ID"  as customer_id,
	                        concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as customer_name, 
	                        lo.redmine_updated_at,
	                        lo.subscription_id as order_subscription_id,
                            lo.redmine_issue_number,
	                        ms."SUBSCRIPTION_ID" as subscription_id,
	                        ms."START"::date as start_date,
	                        -- adjust cancellation date
							(
                             case
                                 when ms."START" > ms."CANCELLATION_DATE" then ms."START" 
                                else ms."CANCELLATION_DATE" 
                              end
                            )::date as end_date,
	                        ms."STATUS" as subscription_status,
	                        ms."ARTICLE_NUMBER" as article_number,
	                        -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as product,
	                        lo.progress 
			  from oro_mysql.libify__order lo 
			  left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
              left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
              left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
              /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                */
              where mc."CUSTOMER_ID" notnull and 
                     ms."STATUS" in ('active', 'trial') and 
                     ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906')
	        ),
	cte2 as (
	          select 
	                distinct 
	                        -- items_serial_numbers,
	                        device_type,
	                        serial_number,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        dense_rank() over w as den_rk_redmine,
	                        -- order_subscription_id,
	                        -- redmine_issue_number,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        article_number,
	                        product,
	                        progress
	          from cte1
	          window w as (partition by customer_id, subscription_id order by redmine_updated_at desc)
	        ),
	cte3 as (
	          select 
	                distinct
	                        device_type,
	                        serial_number,
	                        (
	                         case 
	                         	 when count(serial_number) over w > 1 then 'Ja'
	                          else 'Nein'
	                         end
	                         ) as ist_dupliziert,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        -- den_rk_redmine,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        article_number,
	                        product,
	                        progress
	          from cte2
	          where 
	          -- filter latest redmine timestamp by customer id and subscription id
	          den_rk_redmine = 1
	          window w as (partition by serial_number)
	        ),
	cte4 as (
	          select 
	                distinct 
	                        trim(device_type) as device_types,
	                        trim(cte3.serial_number) as serial_numbers,
	                        -- redmine_updated_at,
	                        ist_dupliziert,
	                        customer_id,
	                        customer_name,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        -- article_number,
	                        product,
	                        progress,
	                        -- adjust device type
	         	           (
	         	            case
	         	                when d."type" = 5 then 'GeoX' 
	         	                when d."type" = 6 then 'Mobile' 
	         	                when d."type" = 12 then 'Sim Card'
	         	                when d."type" = 14 then 'RF Module'
	         	                when d."type" = 17 then 'Radio Button'
	         	                when d."type" = 20 then 'Active Cradle'
	         	                when d."type" = 22 then 'Gateway'
	         	                when d."type" = 25 then 'Sensoric'
	         	                when d."type" = 30 then 'Smart Watch'
	         	                when d."type" = 35 then 'Tablet'
	         	              else null
	         	             end
	         	           ) as device_name,
	                        -- assign device proprietor names
			               (
			                case
			                    when (d.serial_number like '%gc%' or d.serial_number like '%TRC%') and (d.proprietor_id isnull) then 'Libify'
				                when d.proprietor_id = 'e70a076b-3884-474b-b055-c30622379dfb' then 'Libify'
			                    when d.proprietor_id = 'a169b327-6d9e-4fdc-8d0a-a5e84c1e3a39' then 'Customer'
			                    when (d.serial_number like '%LS%') and (d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = 'bd557bf0-b3a0-4e37-b0e9-2c7d6ebf6de1' then 'Arkea'
			                    when (d.serial_number like '%JY%' and d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = '4fcb1ec0-2485-4a5a-9c1c-a22b50f69e8f' then 'MD Medicus'
			                    when d.proprietor_id = '420955f1-65a1-4521-8534-7309ea7edf50' then 'ÖRK Salzburg'
			                    when d.proprietor_id = '6bb64f85-f577-4fb1-aa70-b8fcd4af08d4' then 'Kineo'
			                    when d.proprietor_id = '23079d33-d170-437e-b683-56888fc435b2' then 'Libify_ExKineo23'
			                  else null
			                 end
			               ) as proprietor_name,
			               d.created_at::date as device_created_date
	          from cte3
	          left join devices d on cte3.serial_number = d.serial_number
	        )
-- select distinct * from cte1
-- select distinct * from cte2
-- select distinct * from cte3
select distinct * from cte4

         """


query2 = """
         -- returns backend devices and their status for Annual Inventory Reporting -- reporting-db.libify.cloud:5432 (Updated 19.01.2024) 

with
    cte1 as (
              -- returns information about devices 
              select 
                    distinct 
                           d.id as geräte_id,
                           -- extract device types
			                (
			                 case 
			                     when d.serial_number like '%AC%' then left(d.serial_number, 3)
			                     when d.serial_number like '%RB%' then left(d.serial_number, 3)
			                     when d.serial_number like '%GC%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TRC%' then left(d.serial_number, 3)
			                     when d.serial_number like '%LS%' then left(d.serial_number, 2)
			                     when d.serial_number like '%OM%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TMP%' then left(d.serial_number, 3)
			                     when d.serial_number like '%PG%' then left(d.serial_number, 2)
			                     when d.serial_number like '%SP%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TP%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TY%' then left(d.serial_number, 3)
			                     when d.serial_number like '%UT%' then left(d.serial_number, 3)
			                     when d.serial_number like '%GW%' then left(d.serial_number, 3)
			                     when d.serial_number like '%GZ%' then left(d.serial_number, 3)
			                     when d.serial_number like '%JY%' then left(d.serial_number, 2)
			                     when d.serial_number like '%SY%' then left(d.serial_number, 2)
			                     when d.serial_number like '%gc%' then upper(left(d.serial_number, 3))
			                   end 
			                ) as Gerätetyp,
                           d.serial_number,
                           -- adjust device kind
	         	           (
	         	             case
	         	                  when d."type" = 5 then 'GeoX' 
	         	                  when d."type" = 6 then 'Mobile' 
	         	                  when d."type" = 12 then 'Sim Card'
	         	                  when d."type" = 14 then 'RF Module'
	         	                  when d."type" = 17 then 'Radio Button'
	         	                  when d."type" = 20 then 'Active Cradle'
	         	                  when d."type" = 22 then 'Gateway'
	         	                  when d."type" = 25 then 'Sensoric'
	         	                  when d."type" = 30 then 'Smart Watch'
	         	                  when d."type" = 35 then 'Tablet'
	         	                else null
	         	              end
	         	             ) as Gerätename,
	         	             -- assign device proprietor names
			                 (
			                   case
			                       when (d.serial_number like '%gc%' or d.serial_number like '%TRC%') and (d.proprietor_id isnull) then 'Libify'
				                   when d.proprietor_id = 'e70a076b-3884-474b-b055-c30622379dfb' then 'Libify'
			                       when d.proprietor_id = 'a169b327-6d9e-4fdc-8d0a-a5e84c1e3a39' then 'Customer'
			                       when (d.serial_number like '%LS%') and (d.proprietor_id isnull) then 'Arkea'
			                       when d.proprietor_id = 'bd557bf0-b3a0-4e37-b0e9-2c7d6ebf6de1' then 'Arkea'
			                       when (d.serial_number like '%JY%' and d.proprietor_id isnull) then 'Arkea'
			                       when d.proprietor_id = '4fcb1ec0-2485-4a5a-9c1c-a22b50f69e8f' then 'MD Medicus'
			                       when d.proprietor_id = '420955f1-65a1-4521-8534-7309ea7edf50' then 'ÖRK Salzburg'
			                       when d.proprietor_id = '6bb64f85-f577-4fb1-aa70-b8fcd4af08d4' then 'Kineo'
			                       when d.proprietor_id = '23079d33-d170-437e-b683-56888fc435b2' then 'Libify_ExKineo23'
			                    else null
			                  end
			                ) as Inhaber,
                           d.created_at::date as Gerät_erstellt_Datum
              from devices d
              	-- filter only Geox, Mobiles, and Tablets
              where d."type" in (5, 6, 35) and 
              	-- filter GC, LS, TRC, JY and SY devices 
              	d.serial_number like '%GC%' or 
              	d.serial_number like '%LS%' or 
                d.serial_number like '%TRC%' or 
                d.serial_number like '%JY%' or
                d.serial_number like '%SY%'
            ),
	cte2 as (
	          -- define rückläufer: if device id as minimum one time the status "returned by customer"
	          select 
	                distinct 
	                        dl.device_id,
	                        dl.device_identifier
	          from device_lifecycles dl
	          -- filter only devices with status "returned by customer"
	          where dl.status = 'returned_by_customer'
	        ),
	cte3 as (
	          -- take the latest device created date for sales information
	          select 
	                distinct 
	                        dl.device_id,
	                        dl.device_serial,
	                        max(dl.created_at::date) as max_sales_date
	         from device_lifecycles dl
	         where dl."type" = 'salesInformation'
			 group by 1, 2
	        ),
	cte4 as (
	          -- take the latest sales status for each device sales information based on it's timestamp
			  select 
      				distinct 
              				sales_status.device_id,
              				sales_status.device_serial,
              				sales_status.sales_status,
              				sales_status.sales_date::date
			  from (
	   				 select 
	         			   distinct 
	                               dl.device_id,
	                               dl.device_serial,
	                 			   dl.status as sales_status,
	                               dl.created_at as sales_date,
	                               dense_rank() over w1 as status_date_rk
                     from device_lifecycles dl
                     where dl."type" = 'salesInformation'
                     -- group devices based on their id's and serial numbers and ordered by the status created date
                     window w1 as (partition by dl.device_id, dl.device_serial order by dl.created_at desc)
                   ) as sales_status
                   -- filter table based on the latest status date for each device
                   where sales_status.status_date_rk = 1
	        ),
	cte5 as (
	          -- take the latest device created date for service information
	          select 
	                distinct 
	                        dl.device_id,
	                        dl.device_serial,
	                        max(dl.created_at::date) as max_service_date
	         from device_lifecycles dl
	         where dl."type" = 'serviceInformation'
			 group by 1, 2
	        ),
	cte6 as (
	          -- take the latest service status for each device service information based on it's timestamp
			  select 
                    distinct 
                            service_status.device_id,
                            service_status.device_serial,
                            service_status.service_status,
                            service_status.service_date::date
              from (
	                 select 
	                       distinct 
	                               dl.device_id,
	                               dl.device_serial,
	                               dl.status as service_status,
	                               dl.created_at as service_date,
	                               dense_rank() over w2 as status_date_rk
                     from device_lifecycles dl
                     where dl."type" = 'serviceInformation'
                     -- group devices based on their id's and serial numbers and ordered by the status created date
                     window w2 as (partition by dl.device_id, dl.device_serial order by dl.created_at desc)
                   ) as service_status
                   -- filter table based on the latest status date for each device
                   where service_status.status_date_rk = 1
	        ),
	final_tbl as (
	               -- merge all temporary tables
	               select
	                     distinct 
	                             cte1.geräte_id,
	                             cte1.gerätetyp,
	                             cte1.serial_number,
	                             cte1.gerätename,
	                             cte1.inhaber,
	                             -- define whether a device is old or new
	                             (
	                               case 
	                               	   when cte2.device_identifier isnull then 'Neugerät' 
		                             else 'Rückläufer' 
		                           end
	                             ) as Gerätezustand,
	                             cte3.max_sales_date as Verkaufsdatum,
	                             cte4.sales_status as Verkaufsstatus,
	                             cte5.max_service_date as Servicedatum,
	                             cte6.service_status as Servicestatus,
	                             -- define status for operations
	                             (
	                               case
	                               	   when (cte4.sales_status in ('b2c_sales', 'b2c_rental', 'b2b_sales', 'b2b_rental')) and (cte6.service_status = 'delivery_out')
	                               	        then 'Beim Kunden'
	                               	   when (cte4.sales_status = 'undefined') and (cte6.service_status = 'out_of_service') then 'Auf Lager - Kaputt'
	                               	   when (cte4.sales_status = 'returned_by_customer') and (cte6.service_status = 'store_libify')
	                               	   		then 'Auf Lager - Funktionsfähig - Zurückgeschickt'
	                               	   when (cte4.sales_status = 'undefined') and (cte6.service_status = 'store_libify') then 'Neugerät'
	                               	   when (cte4.sales_status = 'testdevice') and (cte6.service_status = 'store_libify') then 'Testgerät - Lager'
	                               	   when (cte4.sales_status = 'testdevice') and (cte6.service_status = 'delivery_out') then 'Testgerät - Kunde'
	                               	   when (cte4.sales_status = 'undefined') and (cte6.service_status = 'lost') then 'Verlorenes Gerät - Libify'
	                               	   when (cte4.sales_status in ('b2c_rental', 'b2b_rental')) and (cte6.service_status = 'lost') then 'Verlorenes Gerät - Kunde'
	                               	   when (cte4.sales_status = 'undefined') and (cte6.service_status = 'service_production') 
	                               	   		then 'Von Libify zurückgeschickte Geräte'
	                               	   when (cte4.sales_status = 'undefined') and (cte6.service_status = 'store_production  ') 
	                               	   		then 'Geräte die uns von z.B Arkea zugeschickt werden'
	                               	   when (cte4.sales_status = 'b2b_sales') and (cte6.service_status = 'store_libify') then 'Reparaturgeräte'
	                               	else null
	                               end
	                             ) as Betriebszustand,
	                             cte1.gerät_erstellt_datum
	               from cte1
	               left join cte2 on cte1.geräte_id = cte2.device_id
	               left join cte3 on cte1.geräte_id = cte3.device_id and cte1.serial_number = cte3.device_serial
	               left join cte4 on cte1.geräte_id = cte4.device_id and cte1.serial_number = cte4.device_serial
	               left join cte5 on cte1.geräte_id = cte5.device_id and cte1.serial_number = cte5.device_serial
	               left join cte6 on cte1.geräte_id = cte6.device_id and cte1.serial_number = cte6.device_serial
	             )
-- select distinct * from cte1
-- select distinct * from cte2
-- select distinct * from cte3	        
-- select distinct * from cte4
-- select distinct * from cte5
-- select distinct * from cte6	        
select distinct * from final_tbl

         """