query1 = """
				 -- Returns all Kineo and Libify_ExKineo23 Devices + Their Statuses Both With Customers and In Backend -- reporting-db.libify.cloud:5432 (Updated 23.01.2024)

with devices as (
                  select 
                        distinct 
								d.serial_number, 
	                            d.created_at,
	                            sales_status.sales_status, 
	                            service_status.service_status, 
	                            sales_status.max_sales_date::date as sales_date, 
	                            service_status.max_service_date::date as service_date,
	                            d.proprietor_id ,
	                            (
	                             case 
		                             when rl.device_identifier isnull then 'Neugerät' 
		                            else 'Rückläufer' 
		                          end
		                         ) as device_type 
                  from devices d 
                  -- define rückläufer: if device id as minimum one time the status "returned by customer"
                  left join (
	                          select 
	                                distinct 
	                                        dl.device_identifier 
	                          from device_lifecycles dl 
	                          where status = 'returned_by_customer'
                             ) as rl on rl.device_identifier = d.identifier 
				  left join 
						   (
						     select 
						           distinct 
	                                       sales_date.device_id, 
	                                       sales_date.device_serial, 
	                                       sales_date.max_created_date as max_sales_date, 
	                                       sales_info.status as sales_status
	                         from (
	                                select 
		                                  device_id, 
		                                  device_serial, 
		                                  max(created_at) as max_created_date
		                            from device_lifecycles  
		                            where "type" = 'salesInformation' 
		                            group by 1, 2
		                          ) as sales_date
	               left join (
	                           select 
									 device_id , 
		                             device_serial, 
		                             status, 
		                             created_at 
		                       from device_lifecycles  
		                       where "type" = 'salesInformation'
		                     ) as sales_info on sales_date.device_id = sales_info.device_id  and 
		                          sales_date.max_created_date = sales_info.created_at and 
		                          sales_date.device_serial = sales_info.device_serial
	                          ) as sales_status on sales_status.device_id = d.id and d.serial_number = sales_status.device_serial
                   left join 
	                        (
	                          select 
	                                distinct 
	                                        service_date.device_id, 
	                                        service_date.device_serial, 
	                                        service_date.max_created_date as max_service_date, 
	                                        service_info.status as service_status
	                          from (
	                                 select 
		                                   device_id, 
		                                   device_serial, 
		                                   max(created_at) as max_created_date
		                             from device_lifecycles  
		                             where "type" = 'serviceInformation' 
		                             group by 1, 2
		                           ) as service_date
	                          left join (
	                                      select 
		                                        device_id, 
		                                        device_serial, 
		                                        status, 
		                                        created_at 
		                                  from device_lifecycles  
		                                  where "type" = 'serviceInformation'
		                                 ) as service_info on service_date.device_id = service_info.device_id and 
		                                      service_date.max_created_date = service_info.created_at and 
		                                      service_date.device_serial = service_info.device_serial
	                                     ) as service_status on service_status.device_id = d.id and d.serial_number = service_status.device_serial 
                              where d.serial_number like '%GC%' or 
                                    d.serial_number like '%LS%' or 
                                    d.serial_number like '%TRC%' or 
                                    d.serial_number like '%JY%' or
                                    d.serial_number like '%SY%'
                ),
	kineo as (
	           select 
	                 distinct 
	                         serial_number as Seriennummer,
	                         sales_status as Verkaufsstatus,
	                         service_status as Servicestatus,
	                         sales_date as Verkaufsdatum,
	                         service_date as Servicedatum,
	                         -- proprietor_id,
	                         -- assign proprietor names
	                         (
	                           case 
	                           	   when proprietor_id = '6bb64f85-f577-4fb1-aa70-b8fcd4af08d4' then 'Kineo'
			                       when proprietor_id = '23079d33-d170-437e-b683-56888fc435b2' then 'Libify_ExKineo23'
	                           end
	                         ) as Inhaber,
	                         device_type as Gerätezustand,
	                         created_at
	           from devices
	           -- filter Kineo and Libify_ExKineo23 devices
	           where proprietor_id in ('6bb64f85-f577-4fb1-aa70-b8fcd4af08d4', '23079d33-d170-437e-b683-56888fc435b2')
	         ),
	cte1 as (
	          -- Returns Libify Customers For All Products (Updated 18.01.2024)
	          select
	                distinct 
                            -- replace empty serial numbers with null
	                        replace(lo.items_serial_numbers, '[""]', null) as items_serial_numbers,
	                        -- extract device type
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then right(split_part(items_serial_numbers, '-', 3), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(left(split_part(items_serial_numbers, ',', 2), 4), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then substring(split_part(items_serial_numbers, ',', 2), 2, 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
	               		    	when items_serial_numbers like '%GC%' then trim(split_part(items_serial_numbers, '-', 1), '["')
	               		    	when items_serial_numbers like '%SM%' then right(split_part(items_serial_numbers, '-', 1), 2)
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then right(split_part(items_serial_numbers, '-', 1), 2) 
	               		    	when items_serial_numbers like '%SN%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%LS%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%TRC%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '["RB%'then right(split_part(items_serial_numbers, '-', 1), 3)
	               		    	when items_serial_numbers like '["RB%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '%JY%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%SY%' then substring(items_serial_numbers, 3, 2)
	               		    end
	               		   ) as device_type,
	               		   -- extract serial numbers
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')  
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then trim(substring(items_serial_numbers, 10, 11), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%GC%' then trim(substring(items_serial_numbers, 3, 10), '"\][,')
	               		    	when items_serial_numbers like '%SM%' then trim(substring(items_serial_numbers, 3, 11), '"\,')
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%SN%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%LS%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    	when items_serial_numbers like '%TRC%' then trim(substring(items_serial_numbers, 3, 14), '"\][,')
	               		    	when items_serial_numbers like '["RB%' then trim(substring(items_serial_numbers, 3, 12), '"\][,')
	               		    	when items_serial_numbers like '%JY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%SY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    end
	               		    ) as serial_number,
	                        mc."CUSTOMER_ID"  as customer_id,
	                        concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as customer_name, 
	                        lo.redmine_updated_at,
	                        lo.subscription_id as order_subscription_id,
                            lo.redmine_issue_number,
	                        ms."SUBSCRIPTION_ID" as subscription_id,
	                        ms."START"::date as start_date,
	                        -- adjust cancellation date
							(
                             case
                                 when ms."START" > ms."CANCELLATION_DATE" then ms."START" 
                                else ms."CANCELLATION_DATE" 
                              end
                            )::date as end_date,
	                        ms."STATUS" as subscription_status,
	                        ms."ARTICLE_NUMBER" as article_number,
	                        -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                            */
	                        (
	                         case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as product,
	                        lo.progress 
			  from oro_mysql.libify__order lo 
			  left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
              left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
              left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
              /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
              */
              where mc."CUSTOMER_ID" notnull and 
                     ms."STATUS" in ('active', 'trial') and 
                     ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906')
	        ),
	cte2 as (
	          select 
	                distinct 
	                        -- items_serial_numbers,
	                        device_type,
	                        serial_number,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        dense_rank() over w as den_rk_redmine,
	                        -- order_subscription_id,
	                        -- redmine_issue_number,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        article_number,
	                        product,
	                        progress
	          from cte1
	          window w as (partition by customer_id, subscription_id order by redmine_updated_at desc)
	        ),
	cte3 as (
	          select 
	                distinct
	                        device_type,
	                        serial_number,
	                        (
	                         case 
	                         	 when count(serial_number) over w > 1 then 'Ja'
	                          else 'Nein'
	                         end
	                         ) as ist_dupliziert,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        -- den_rk_redmine,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        article_number,
	                        product,
	                        progress
	          from cte2
	          where 
	          -- filter latest redmine timestamp by customer id and subscription id
	          den_rk_redmine = 1
	          window w as (partition by serial_number)
	        ),
	cte4 as (
	          select 
	                distinct 
	                        -- trim(device_type) as device_types,
	                        trim(cte3.serial_number) as Seriennummer,
	                        -- redmine_updated_at,
	                        -- ist_dupliziert,
	                        customer_id as Kunden_ID,
	                        customer_name as Kundenname,
	                        subscription_id as Abo_ID,
	                        start_date as Startdatum,
	                        -- end_date,
	                        subscription_status as Abo_Status,
	                        -- article_number,
	                        product as Produkt 
	                        -- progress
	          from cte3
	          left join devices d on cte3.serial_number = d.serial_number
	        ),
	final_tbl as (
	               select 
	                     distinct
	                             kundenname,
	                             kunden_id,
	                             abo_id,
	                             startdatum,
	                             /*
	                              Since there are active customers with duplicated serial numbers, remove duplicates by
	                              Step_1: Take the latest subscription start date per serial number
	                              Step_2: Remove duplicates using the latest subscription start per device as a filter
	                             */
	                             dense_rank () over (partition by kineo.seriennummer order by startdatum desc) as rk,
	                             abo_status,
	                             produkt,
	                             -- extract device types
			                     (
			                 	   case 
			                     	   when kineo.seriennummer like '%AC%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%RB%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%GC%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%TRC%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%LS%' then left(kineo.seriennummer, 2)
			                     	   when kineo.seriennummer like '%OM%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%TMP%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%PG%' then left(kineo.seriennummer, 2)
			                     	   when kineo.seriennummer like '%SP%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%TP%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%TY%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%UT%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%GW%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%GZ%' then left(kineo.seriennummer, 3)
			                     	   when kineo.seriennummer like '%JY%' then left(kineo.seriennummer, 2)
			                     	   when kineo.seriennummer like '%SY%' then left(kineo.seriennummer, 2)
			                     	   when kineo.seriennummer like '%gc%' then upper(left(kineo.seriennummer, 3))
			                   		  end 
			                	 ) as Gerätetyp,
	                             kineo.seriennummer,
	                             verkaufsdatum,
	                             verkaufsstatus,
	                             servicedatum,
	                             servicestatus,
	                             inhaber,
	                             gerätezustand,
	                             created_at::date as Gerät_erstellt_Datum    
	               from kineo
	               left join cte4 on kineo.seriennummer = cte4.seriennummer
	             )
-- select distinct * from devices
-- select distinct * from kineo
-- select distinct * from cte4
select distinct * from final_tbl where rk = 1 order by verkaufsdatum desc, servicedatum desc

         """