query1 = """
				 -- Tele2 SIM Cards Cost Analysis (Updated 24.01.2024)  -- reporting-db.libify.cloud:5432

with
    cte1 as (
              select  
                    distinct 
                            -- extract device types
			                (
			                 case 
			                     when d.serial_number like '%AC%' then left(d.serial_number, 3)
			                     when d.serial_number like '%RB%' then left(d.serial_number, 3)
			                     when d.serial_number like '%GC%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TRC%' then left(d.serial_number, 3)
			                     when d.serial_number like '%LS%' then left(d.serial_number, 2)
			                     when d.serial_number like '%OM%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TMP%' then left(d.serial_number, 3)
			                     when d.serial_number like '%PG%' then left(d.serial_number, 2)
			                     when d.serial_number like '%SP%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TP%' then left(d.serial_number, 3)
			                     when d.serial_number like '%TY%' then left(d.serial_number, 3)
			                     when d.serial_number like '%UT%' then left(d.serial_number, 3)
			                     when d.serial_number like '%GW%' then left(d.serial_number, 3)
			                     when d.serial_number like '%GZ%' then left(d.serial_number, 3)
			                     when d.serial_number like '%JY%' then left(d.serial_number, 2)
			                     when d.serial_number like '%SY%' then left(d.serial_number, 2)
			                     when d.serial_number like '%gc%' then upper(left(d.serial_number, 3))
			                   end 
			                ) as device_type,
                            d.serial_number,
                            -- assign device proprietor names
			                (
			                  case
			                      when (d.serial_number like '%gc%' or d.serial_number like '%TRC%') and (d.proprietor_id isnull) then 'Libify'
				                  when d.proprietor_id = 'e70a076b-3884-474b-b055-c30622379dfb' then 'Libify'
			                      when d.proprietor_id = 'a169b327-6d9e-4fdc-8d0a-a5e84c1e3a39' then 'Customer'
			                      when (d.serial_number like '%LS%') and (d.proprietor_id isnull) then 'Arkea'
			                      when d.proprietor_id = 'bd557bf0-b3a0-4e37-b0e9-2c7d6ebf6de1' then 'Arkea'
			                      when (d.serial_number like '%JY%' and d.proprietor_id isnull) then 'Arkea'
			                      when d.proprietor_id = '4fcb1ec0-2485-4a5a-9c1c-a22b50f69e8f' then 'MD Medicus'
			                      when d.proprietor_id = '420955f1-65a1-4521-8534-7309ea7edf50' then 'ÖRK Salzburg'
			                      when d.proprietor_id = '6bb64f85-f577-4fb1-aa70-b8fcd4af08d4' then 'Kineo'
			                      when d.proprietor_id = '23079d33-d170-437e-b683-56888fc435b2' then 'Libify_ExKineo23'
			                   else null
			                 end
			                ) as proprietor_name,
                            -- sc.id,
                            -- sc."source",
                            sc.iccid,
                            sc.imei,
                            -- assign subscription charge for each rate plan
                            /*
                             * Plan Name
                             * 100MB Flexpool - PZ GER/AUS - Libify. (EUR1.00)
                             * 200MB Flexpool - PZ GER/AUS - Libify. (EUR1.30)
                             * 2MB 10SMS 0.5MIN Flex PZ1+10 - Libify (EUR0.62)
                             * Pay as Use - Libify. (EUR0.35)
                            */
                            (
                              case 
                              	  when rate_plan = '100MB Flexpool - PZ GER/AUS - Libify.' then 1
                              	  when rate_plan = '200MB Flexpool - PZ GER/AUS - Libify.' then 1.30
                              	  when rate_plan = '2MB 10SMS 0.5MIN Flex PZ1+10 - Libify' then 0.62
                              	  when rate_plan = 'Pay as Use - Libify.' then 0.35
                              	else null
                              end
                            ) as sim_abo_charge, 
                            -- assign rate plan information for each sim card
                            /*
                             * Plan Name
                             * 100MB Flexpool - PZ GER/AUS - Libify. (included = data => 100MB)
                             * 200MB Flexpool - PZ GER/AUS - Libify. (included = data => 200MB)
                             * 2MB 10SMS 0.5MIN Flex PZ1+10 - Libify (included = data => 2MB, SMS => 10, Call => 0.5MIN)
                             * Pay as Use - Libify.
                            */
                            -- extract sim allowed maximum data
                            (
                              case 
                              	  when rate_plan = '100MB Flexpool - PZ GER/AUS - Libify.' then 100
                              	  when rate_plan = '200MB Flexpool - PZ GER/AUS - Libify.' then 200
                              	  when rate_plan = '2MB 10SMS 0.5MIN Flex PZ1+10 - Libify' then 2
                              	else null
                              end
                            ) as sim_max_data,
                            -- extract sim allowed maximum sms
                            (
                              case 
                              	  when rate_plan = '2MB 10SMS 0.5MIN Flex PZ1+10 - Libify' then 10
                              	else null
                              end
                            ) as sim_max_sms,
                            -- extract sim allowed voice call
                            (
                              case 
                              	  when rate_plan = '2MB 10SMS 0.5MIN Flex PZ1+10 - Libify' then 0.5
                              	else null
                              end
                            ) as sim_max_voice,
                            -- sc.rate_plan,
                            /*
                               convert bytes to megabytes 
                               1,000,000 bytes ==> 1MB
                            */
                            div(sc.data_usage_bytes, 1000000) as data_used_mb,
                            -- sc.data_usage_bytes,
                            /*
                               convert voice in seconds to minutes 
                               60 seconds ==> 1MIN
                            */
                            div(sc.voice_usage_originated_seconds, 60) as voice_used_mins,
                            -- sc.voice_usage_originated_seconds,
                            sc.sms_usage_originated as sms_sent,
                            sc.updated_at::date as sim_card_date
              from public.sim_cards sc
              left join devices d on sc.imei = d.imei_number
              /*
               * consider only activated sim cards within the current month
               * consider only TELE2_JASPER_CC devices
              */
              where sc.status = 'ACTIVATED' and
                    sc."source" = 'TELE2_JASPER_CC'
             ),
	cte2 as (
	          select 
	                distinct 
	                        device_type,
	                        serial_number,
	                        iccid,
	                        -- create a unique row grouped based on each unique sim iccid
	                        count(iccid) over w1 as sim_rn,
	                        imei,
	                        proprietor_name,
	                        sim_abo_charge,
	                        sim_max_data,
	                        data_used_mb,
	                        -- calculate sim remaining data
	                        (sim_max_data - data_used_mb) as sim_data_rem,
	                        sim_max_sms,
	                        sms_sent,
	                        -- calculate sim remaining sms
	                        (sim_max_sms - sms_sent) as sim_sms_rem,
	                        sim_max_voice,
	                        voice_used_mins,
	                        -- calculate sim remining voice
	                        (sim_max_voice - voice_used_mins) as sim_voice_rem,
	                        sim_card_date
	          from cte1
	          window w1 as (partition by iccid)
	        ),
	cte3 as (
	          select 
	                distinct 
	                        device_type,
	                        serial_number,
	                        iccid,
	                        sim_rn,
	                        imei,
	                        proprietor_name,
	                        sim_abo_charge,
	                        sim_max_data,
	                        data_used_mb,
	                        sim_data_rem,
	                        /*
	                         * calculate sim data overcharge cost
	                         * Data ==> 0.40 € / MB
	                        */
	                        (
	                          case
	                          	  when (sim_data_rem < 0) then (sim_data_rem * 0.40) 
	                          end
	                        ) as data_overcharge_cost,
	                        sim_max_sms,
	                        sms_sent,
	                        sim_sms_rem,
	                        /*
	                         * calculate sim sms overcharge cost
	                         * SMS ==> 0.06 € / SMS
	                        */
	                        (
	                          case
	                          	  when (sim_sms_rem < 0) then (sim_sms_rem * 0.06) 
	                          end
	                        ) as sms_overcharge_cost,
	                        sim_max_voice,
	                        voice_used_mins,
	                        sim_voice_rem,
	                        /*
	                         * calculate sim voice overcharge cost
	                         * Voice ==> 0.19 € / Minute
	                        */
	                        (
	                          case
	                          	  when (sim_voice_rem < 0) then (sim_voice_rem * 0.19) 
	                          end
	                        ) as voice_overcharge_cost,
	                        sim_card_date
	          from cte2
	        ),
	cte4 as (
	          -- Returns Libify Customers For All Products (Updated 23.01.2024)
	          select
	                distinct 
                            -- replace empty serial numbers with null
	                        replace(lo.items_serial_numbers, '[""]', null) as items_serial_numbers,
	                        -- extract device type
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then right(split_part(items_serial_numbers, '-', 3), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(left(split_part(items_serial_numbers, ',', 2), 4), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then substring(split_part(items_serial_numbers, ',', 2), 2, 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
	               		    	when items_serial_numbers like '%GC%' then trim(split_part(items_serial_numbers, '-', 1), '["')
	               		    	when items_serial_numbers like '%SM%' then right(split_part(items_serial_numbers, '-', 1), 2)
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then right(split_part(items_serial_numbers, '-', 1), 2) 
	               		    	when items_serial_numbers like '%SN%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%LS%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%TRC%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '["RB%'then right(split_part(items_serial_numbers, '-', 1), 3) 
	               		    	when items_serial_numbers like '["RB%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '%JY%' then substring(items_serial_numbers, 3, 2)
	               		    end
	               		   ) as device_type,
	               		   -- extract serial numbers
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')  
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then trim(substring(items_serial_numbers, 10, 11), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%GC%' then trim(substring(items_serial_numbers, 3, 10), '"\][,')
	               		    	when items_serial_numbers like '%SM%' then trim(substring(items_serial_numbers, 3, 11), '"\,')
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%SN%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%LS%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    	when items_serial_numbers like '%TRC%' then trim(substring(items_serial_numbers, 3, 14), '"\][,')
	               		    	when items_serial_numbers like '["RB%' then trim(substring(items_serial_numbers, 3, 12), '"\][,')
	               		    	when items_serial_numbers like '%JY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    end
	               		    ) as serial_number,
	                        mc."CUSTOMER_ID"  as customer_id,
	                        concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as customer_name, 
	                        lo.redmine_updated_at,
	                        lo.subscription_id as order_subscription_id,
                            lo.redmine_issue_number,
	                        ms."SUBSCRIPTION_ID" as subscription_id,
	                        ms."START"::date as start_date,
	                        -- adjust cancellation date
							(
                             case
                                 when ms."START" > ms."CANCELLATION_DATE" then ms."START" 
                                else ms."CANCELLATION_DATE" 
                              end
                            )::date as end_date,
	                        ms."STATUS" as subscription_status,
	                        ms."ARTICLE_NUMBER" as article_number,
	                        -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as product,
	                        lo.progress 
			  from oro_mysql.libify__order lo 
			  left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
              left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
              left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
              /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                */
              where mc."CUSTOMER_ID" notnull and 
                     ms."STATUS" in ('active', 'trial') and 
                     ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906')
	        ),
	cte5 as (
	          select 
	                distinct 
	                        -- items_serial_numbers,
	                        device_type,
	                        serial_number,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        dense_rank() over w as den_rk_redmine,
	                        -- order_subscription_id,
	                        -- redmine_issue_number,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        article_number,
	                        product,
	                        progress
	          from cte4
	          window w as (partition by customer_id, subscription_id order by redmine_updated_at desc)
	        ),
	cte6 as (
	          select 
	                distinct
	                        device_type,
	                        serial_number,
	                        (
	                         case 
	                         	 when count(serial_number) over w > 1 then 'Ja'
	                          else 'Nein'
	                         end
	                         ) as ist_dupliziert,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        -- den_rk_redmine,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        article_number,
	                        product,
	                        progress
	          from cte5
	          where 
	          -- filter latest redmine timestamp by customer id and subscription id
	          den_rk_redmine = 1
	          window w as (partition by serial_number)
	        ),
	cte7 as (
	          select 
	                distinct 
	                        trim(device_type) as device_types,
	                        trim(cte6.serial_number) as serial_numbers,
	                        -- redmine_updated_at,
	                        ist_dupliziert,
	                        customer_id,
	                        customer_name,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        subscription_status,
	                        -- article_number,
	                        product,
	                        progress,
	                        -- adjust device type
	         	           (
	         	            case
	         	                when d."type" = 5 then 'GeoX' 
	         	                when d."type" = 6 then 'Mobile' 
	         	                when d."type" = 12 then 'Sim Card'
	         	                when d."type" = 14 then 'RF Module'
	         	                when d."type" = 17 then 'Radio Button'
	         	                when d."type" = 20 then 'Active Cradle'
	         	                when d."type" = 22 then 'Gateway'
	         	                when d."type" = 25 then 'Sensoric'
	         	                when d."type" = 30 then 'Smart Watch'
	         	                when d."type" = 35 then 'Tablet'
	         	              else null
	         	             end
	         	           ) as device_name,
	                        -- assign device proprietor names
			               (
			                case
			                    when (d.serial_number like '%gc%' or d.serial_number like '%TRC%') and (d.proprietor_id isnull) then 'Libify'
				                when d.proprietor_id = 'e70a076b-3884-474b-b055-c30622379dfb' then 'Libify'
			                    when d.proprietor_id = 'a169b327-6d9e-4fdc-8d0a-a5e84c1e3a39' then 'Customer'
			                    when (d.serial_number like '%LS%') and (d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = 'bd557bf0-b3a0-4e37-b0e9-2c7d6ebf6de1' then 'Arkea'
			                    when (d.serial_number like '%JY%' and d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = '4fcb1ec0-2485-4a5a-9c1c-a22b50f69e8f' then 'MD Medicus'
			                    when d.proprietor_id = '420955f1-65a1-4521-8534-7309ea7edf50' then 'ÖRK Salzburg'
			                    when d.proprietor_id = '6bb64f85-f577-4fb1-aa70-b8fcd4af08d4' then 'Kineo'
			                    when d.proprietor_id = '23079d33-d170-437e-b683-56888fc435b2' then 'Libify_ExKineo23'
			                  else null
			                 end
			               ) as proprietor_name,
			               d.created_at::date as device_created_date
	          from cte6
	          left join devices d on cte6.serial_number = d.serial_number
	        ),
	cte8 as (
	          -- join both sim card and customer data information
	          select  
	                distinct 
	                        cte7.customer_id,
	                        cte7.customer_name,
	                        cte7.product,
	                        cte7.device_name,
	                        cte3.*
	          from cte3
	          left join cte7 on cte3.serial_number = cte7.serial_numbers
	        )
-- select distinct * from cte1
-- select distinct * from cte2 
-- select distinct * from cte3  -- returns activated sim card information
-- select distinct * from cte4
-- select distinct * from cte5
-- select distinct * from cte6
-- select distinct * from cte7	  -- returns active & trial customers and their devices information
select distinct * from cte8
	                    
         """