query1 = """
				 -- Returns All LSG Customers For All Products and Their Corresponding Insurance (Updated 06.02.2024)  reporting-db.libify.cloud:5432

with
    cte1 as (
              select    
                    distinct
                            concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as Kundennamen,
                            mc."CUSTOMER_ID" as Kunden_ID,
                            concat(mc."ADDRESS", ' , ', mc."ZIPCODE", ' ', mc."CITY", ' , ', mc."COUNTRY_CODE") as Kundenanschrift,
                            ms."SUBSCRIPTION_ID" as Abo_ID,
                            ms."START"::date as Abo_Start,
                            ms."STATUS" as Abo_Status,
                            -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              end
                            ) as Produkt,
                            
                            oso.is_belong_lsg
               from oro_mysql.monsum__subscription ms
               left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
               left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
               left join oro_mysql.orocrm_sales_opportunity oso on ms."SUBSCRIPTION_ID" = oso.monsum_subscription_id 
               /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                * include only customers with lsg flag as TRUE, thus oso.is_belong_lsg = 1
                */
               where mc."CUSTOMER_ID" notnull and 
                     ms."STATUS" in ('active', 'trial') and 
                     ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906') and
                     oso.is_belong_lsg = 1
             ),
	cte2 as (
	         
	          select
	  				distinct 
	                        mc."CUSTOMER_ID" as customer_id,
	                        ms."STATUS",
	                        mc."CUSTOMER_TYPE",
	                        ms."COUPON_CODE",
	                        (
                              case
	                              when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" = 'KK12-75' then 'Beihilfe'
	                              when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" = 'KK25-50' then 'Zuzahler'
                                  when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" = 'KK100' then 'Zuschuss durch die Pflegekasse'
                                  when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" isnull then 'Selbstzahler'
                               end
                            ) as Zahlungsart        
              from oro_mysql.monsum__subscription ms
              left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
              left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
              /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                * filter only coupon codes ==> 'KK12-75', 'KK25-50', 'KK100' and missing codes
              */
              where mc."CUSTOMER_ID" notnull and 
                    ms."STATUS" in ('active', 'trial') and 
                    ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906') and
                    mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" isnull or ms."COUPON_CODE" in ('KK12-75', 'KK25-50', 'KK100')
	        ),
	cte3 as (
	          select 
	                distinct 
	                        cte1.*,
	                        cte2."CUSTOMER_TYPE",
	                        cte2."COUPON_CODE",
	                        cte2.zahlungsart
	          from cte1
	          left join cte2 on cte1.kunden_id = cte2.customer_id
	        )
-- select distinct * from cte1 order by abo_start desc  -- returns all lsg customers
-- select distinct * from cte2 -- returns customer insurance type
select distinct * from cte3 order by abo_start desc

         """



query2 = """
         -- Returns All Non-LSG Customers For All Products and Their Corresponding Insurance (Updated 06.02.2024)  reporting-db.libify.cloud:5432

with
    cte1 as (
              select    
                    distinct
                            concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as Kundennamen,
                            mc."CUSTOMER_ID" as Kunden_ID,
                            concat(mc."ADDRESS", ' , ', mc."ZIPCODE", ' ', mc."CITY", ' , ', mc."COUNTRY_CODE") as Kundenanschrift,
                            ms."SUBSCRIPTION_ID" as Abo_ID,
                            ms."START"::date as Abo_Start,
                            ms."STATUS" as Abo_Status,
                            -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
                            ) as Produkt,
                            oso.is_belong_lsg
               from oro_mysql.monsum__subscription ms
               left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
               left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
               left join oro_mysql.orocrm_sales_opportunity oso on ms."SUBSCRIPTION_ID" = oso.monsum_subscription_id 
               /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                * include only customers with lsg flag as FALSE, thus oso.is_belong_lsg = 0
                */
               where mc."CUSTOMER_ID" notnull and 
                     ms."STATUS" in ('active', 'trial') and 
                     ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906') and
                     oso.is_belong_lsg = 0
             ),
	cte2 as (
	          select
	  				distinct 
	                        mc."CUSTOMER_ID" as customer_id,
	                        ms."STATUS",
	                        mc."CUSTOMER_TYPE",
	                        ms."COUPON_CODE",
	                        (
                              case
	                              when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" = 'KK12-75' then 'Beihilfe'
	                              when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" = 'KK25-50' then 'Zuzahler'
                                  when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" = 'KK100' then 'Zuschuss durch die Pflegekasse'
                                  when mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" isnull then 'Selbstzahler'
                               end
                            ) as Zahlungsart        
              from oro_mysql.monsum__subscription ms
              left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
              left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
              /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                * filter only coupon codes ==> 'KK12-75', 'KK25-50', 'KK100' and missing codes
              */
              where mc."CUSTOMER_ID" notnull and 
                    ms."STATUS" in ('active', 'trial') and 
                    ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906') and 
                    mc."CUSTOMER_TYPE" = 'consumer' and ms."COUPON_CODE" isnull or ms."COUPON_CODE" in ('KK12-75', 'KK25-50', 'KK100')
	        ),
	cte3 as (
	          select 
	                distinct 
	                        cte1.*,
	                        cte2."CUSTOMER_TYPE",
	                        cte2."COUPON_CODE",
	                        cte2.zahlungsart
	          from cte1
	          left join cte2 on cte1.kunden_id = cte2.customer_id
	        )
-- select distinct * from cte1 order by abo_start desc
-- select distinct * from cte2 -- returns customer insurance type	
select distinct * from cte3 order by abo_start desc        

         """