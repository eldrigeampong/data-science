query1 = """
         -- Duplicates Detection Script (Updated 30.10.2023) ===> CR-Reporting

with 
	cte1 as (
	         select 
	  			   distinct
	                       oa.id as account_id,
	                       oa.name as account_name,
	                       mc."CUSTOMER_ID" as customer_id,
	                       mc."CUSTOMER_NUMBER" as customer_number,
	                       oso.id as opportunity_id,
	                       oso.health_account_pid,
	                       oso.health_account_fid,
	                       oso.status_id as sales_status_id,
	                       ms."STATUS" as subscription_status
			 from orocrm.orocrm_account oa
             left join oro_mysql.orocrm_account_to_contact oatc on oa.id = oatc.account_id
             left join oro_mysql.libify_contact_to_opportunity lcto on lcto.contact_id = oatc.contact_id
             left join oro_mysql.orocrm_contact_role ocr on lcto.role_id = ocr.id
             left join oro_mysql.orocrm_contact oc on oatc.contact_id = oc.id
             left join oro_mysql.orocrm_contact_address oca on oc.id = oca.owner_id 
             left join oro_mysql.orocrm_contact_email oce on oc.id = oce.owner_id 
             left join oro_mysql.orocrm_contact_phone ocp on oc.id = ocp.owner_id 
             left join oro_mysql.orocrm_sales_opportunity oso on lcto.contact_id = oso.contact_id
             left join oro_mysql.monsum__customer mc on oso.monsum_customer_id = mc."CUSTOMER_ID"
             left join oro_mysql.monsum__subscription ms on mc."CUSTOMER_ID" = ms."CUSTOMER_ID" 
             where ms."STATUS" in ('active', 'trial') and oso.status_id in ('acquired', 'won')
          ),
  cte2 as (
           select 
                 distinct 
                         account_id,
                         account_name,
                         customer_id,
                         customer_number,
                         opportunity_id,
                         health_account_pid,
                         health_account_fid,
                         sales_status_id
                         subscription_status
           from cte1
          ),
  cte3 as (
           select
                 distinct
	                     mc."CUSTOMER_ID" as kunden_id,
	                     count(distinct oa.id) as account_count,
	                     count(distinct oso.health_account_pid) as pid_count,
	                     count(distinct oso.health_account_fid) as fid_count
           from orocrm.orocrm_account oa
           left join orocrm.orocrm_account_to_contact oatc on oa.id = oatc.account_id
           left join orocrm.libify_contact_to_opportunity lcto on lcto.contact_id = oatc.contact_id
           left join orocrm.orocrm_contact_role ocr on lcto.role_id = ocr.id
           left join orocrm.orocrm_sales_opportunity oso on lcto.contact_id = oso.contact_id
           left join orocrm.monsum__customer mc on oso.monsum_customer_id = mc."CUSTOMER_ID"
           left join orocrm.monsum__subscription ms on mc."CUSTOMER_ID" = ms."CUSTOMER_ID"
		   where ms."STATUS" in ('active', 'trial') and oso.status_id in ('acquired', 'won')
           group by mc."CUSTOMER_ID"
          ),
  cte4 as (
           select 
                 distinct
                         customer_id,
                         account_id,
                         account_name,
                         customer_number,
                         opportunity_id,
                         subscription_status,
                         health_account_pid,
                         health_account_fid,
                         account_count,
                         pid_count,
                         fid_count
           from cte2
           left join cte3 on cte2.customer_id = cte3.kunden_id
           where 
                -- filter customers with duplicated accounts
                cte3.account_count > 1
          ),
  cte5 as (
	       -- Returns all accounts and contacts activities
	       select
	             distinct 
	                     oa.id as account_id,
	                     oc.id as contact_id,
	                     concat(oc.first_name, ' ', oc.last_name) as contact_name,
	                     oal.id as activity_id,
	                     oal.related_activity_id,
	                     oal.related_activity_class,
	                     oal.subject
           from orocrm.oro_activity_list oal
           left join orocrm.oro_rel_c3990ba6b28b6f38e2d624 oal2oa on oal2oa.activitylist_id = oal.id
           left join orocrm.orocrm_account oa on oa.id = oal2oa.account_id
           left join orocrm.oro_rel_c3990ba683dfdfa4a13cb3 oal2oc on oal2oc.activitylist_id = oal.id
           left join orocrm.orocrm_contact oc on oc.id = oal2oc.contact_id
           left join 
                     (
	                  select
		                    id
	                  from orocrm.orocrm_contact oc
	                  left join orocrm.orocrm_account_to_contact oatc on oatc.contact_id = oc.id
                      ) account_contacts on account_contacts.id = oal2oc.contact_id or oal2oc.contact_id in (account_contacts.id)
		   group by 1, 2, 3, 4
           order by oa.id asc
          ),
  cte6 as (
           select  
                 distinct 
                         customer_id,
                         cte4.account_id,
                         account_count,
                         pid_count,
                         fid_count,
                         account_name,
                         customer_number,
                         opportunity_id,
                         subscription_status,
                         health_account_pid,
                         health_account_fid,
                         activity_id,
                         related_activity_id,
                         related_activity_class,
                         subject            
           from cte4
           left join cte5 on cte4.account_id = cte5.account_id
          )
-- select all columns
select
	  distinct 
               *
from
	cte6
order by
	customer_id,
	account_id
  
         """