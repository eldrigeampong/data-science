query1 = '''
         -- New Customers Prediction (ORO Database) ==> 15.05.2023 

select
	*
from
	(
	select
		distinct 
                    mc.CUSTOMER_ID,
		count(mc.CUSTOMER_ID) as customer_rn,
		mc.CREATED
	from
		oro.monsum__customer mc
	group by
		1
     ) as customer_tbl
where
	customer_rn = 1
order by
	CREATED asc
    
         '''



query2 = '''
         -- Predicting New Leads  ==> ORO Database (31.05.2023)  

select distinct lead_id, status_id, call_center, customer_id, created_date
from (
	   select
	         distinct 
                     oso.lead_id,
	                 osl.status_id,
	                 oso.cc_company_name,
	                 -- create call center groups
	                 (
	                  case
	                  	  when oso.cc_company_name in ('Communication Factory GmbH', 'Communicationfactory') then 'Communication Factory GmbH'
	                  	  when oso.cc_company_name in ('EPFortuna', 'EPFORTUNAOnLibifyLeads', 'sale_epfortuna_new') then 'Epfortuna'
	                  	  when oso.cc_company_name in ('km-co', 'KmCo') then 'Km-Co' 
	                  	  when oso.cc_company_name in ('NeukundeAnlegen', 'Neukunden') then 'Neukunden'
	                  	  when oso.cc_company_name in ('Power-Sales-Service', 'PowerSalesService') then 'PowerSalesService'
	                  	  when lower(oso.cc_company_name) in ('4058', '4154', '4325', '4346', '4368', 'covendos', 'Covendos') then 'Covendos'
	                  	  when oso.cc_company_name like '%Unknown%' or oso.cc_company_name is null then null
	                  	  when lower(oso.cc_company_name) in ('libify', 'Libify', 'philipp.kollmannsberger', 'yagoerice') then 'Libify Owned & Operated'
	                   else 'others'
	                  end
	                  ) as call_center,
	                 oso.monsum_customer_id as customer_id, 
			         oso.created_at as created_date
		from orocrm_sales_lead osl
        left join orocrm_sales_opportunity oso on osl.id = oso.lead_id
        where oso.lead_id is not null
		group by 1
		order by 1
	  ) as sub_tbl
where status_id = 'qualified'

         '''



query3 = '''
				 -- Predicting Daily Active, Trial and Cancelled Subscriptions (12.06.2023) ==> ORO Database

select
	subscription_id,
	start_date,
	end_date,
	subscription_status,
	subscription_created_date
from
	(
	select
		distinct 
	                ms.SUBSCRIPTION_ID as subscription_id,
		ms.`START` as start_date,
		ms.CANCELLATION_DATE as end_date,
		ms.STATUS as subscription_status,
		ms.created_at as subscription_created_date,
		count(distinct ms.SUBSCRIPTION_ID) as sub_rn
	from
		oro.monsum__subscription ms
	group by
		1
     ) as sub_tbl
where
	sub_rn = 1
order by
	subscription_id,
	subscription_created_date
    
         '''