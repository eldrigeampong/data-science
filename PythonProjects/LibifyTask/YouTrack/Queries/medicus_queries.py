query1 = """
				 -- Returns different customers with duplicated serial numbers (Updated 01.02.2024)

with
	cte1 as (
	          select
	                distinct 
                            -- replace empty serial numbers with null
	                        replace(lo.items_serial_numbers, '[""]', null) as items_serial_numbers,
	                        -- extract device type
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then right(split_part(items_serial_numbers, '-', 3), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(left(split_part(items_serial_numbers, ',', 2), 4), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then substring(split_part(items_serial_numbers, ',', 2), 2, 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
	               		    	when items_serial_numbers like '%GC%' then trim(split_part(items_serial_numbers, '-', 1), '["')
	               		    	when items_serial_numbers like '%SM%' then right(split_part(items_serial_numbers, '-', 1), 2)
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then right(split_part(items_serial_numbers, '-', 1), 2) 
	               		    	when items_serial_numbers like '%SN%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%LS%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%TRC%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '["RB%'then right(split_part(items_serial_numbers, '-', 1), 3)
	               		    	when items_serial_numbers like '["RB%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '%JY%' then substring(items_serial_numbers, 3, 2)
	               		    end
	               		   ) as device_type,
	               		   -- extract serial numbers
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')  
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then trim(substring(items_serial_numbers, 10, 11), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%GC%' then trim(substring(items_serial_numbers, 3, 10), '"\][,')
	               		    	when items_serial_numbers like '%SM%' then trim(substring(items_serial_numbers, 3, 11), '"\,')
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%SN%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%LS%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    	when items_serial_numbers like '%TRC%' then trim(substring(items_serial_numbers, 3, 14), '"\][,')
	               		    	when items_serial_numbers like '["RB%' then trim(substring(items_serial_numbers, 3, 12), '"\][,')
	               		    	when items_serial_numbers like '%JY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    end
	               		    ) as serial_number,
	                        mc."CUSTOMER_ID"  as customer_id,
	                        concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as customer_name, 
	                        lo.redmine_updated_at,
	                        lo.subscription_id as order_subscription_id,
                            lo.redmine_issue_number,
	                        ms."SUBSCRIPTION_ID" as subscription_id,
	                        ms."START"::date as start_date,
	                        -- adjust cancellation date
							(
                             case
                                 when ms."START" > ms."CANCELLATION_DATE" then ms."START" 
                                else ms."CANCELLATION_DATE" 
                              end
                            )::date as end_date,
	                        ms."STATUS" as monsum_status,
	                        oso.status_id as opportunity_status,
	                        ri.status_name as redmine_ticket_status,
	                        ms."ARTICLE_NUMBER" as article_number,
	                        -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as product,
	                        ri.done_ratio as redmine_done_ratio,
	                        lo.emergency_call_center 
			  from oro_mysql.libify__order lo
			  left join redmine_issues ri on lo.redmine_issue_number = ri.id 
			  left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
			  left join oro_mysql.orocrm_sales_opportunity oso on ms."SUBSCRIPTION_ID" = oso.monsum_subscription_id 
              left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
              left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
              /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                */
              where mc."CUSTOMER_ID" notnull and 
                     ms."STATUS" in ('active', 'trial') and 
                     ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906')
	        ),
	cte2 as (
	          select 
	                distinct 
	                        -- items_serial_numbers,
	                        device_type,
	                        serial_number,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        dense_rank() over w as den_rk_redmine,
	                        -- order_subscription_id,
	                        -- redmine_issue_number,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        monsum_status,
	                        opportunity_status,
	                        redmine_ticket_status,
	                        article_number,
	                        product,
	                        redmine_done_ratio,
	                        emergency_call_center 
	          from cte1
	          window w as (partition by customer_id, subscription_id order by redmine_updated_at desc)
	        ),
	cte3 as (
	          select
	                distinct 
	                        device_type,
	                        serial_number,
	                        count(serial_number) over w as serial_number_count,
	                        customer_id,
	                        customer_name,
	                        -- redmine_updated_at,
	                        -- den_rk_redmine,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        monsum_status,
	                        opportunity_status,
	                        redmine_ticket_status,
	                        -- article_number,
	                        product,
	                        redmine_done_ratio,
	                        emergency_call_center 
	          from cte2
	          where 
	          -- filter latest redmine timestamp by customer id and subscription id
	          den_rk_redmine = 1
	          window w as (partition by serial_number)
	        )
select
	  distinct 
              *
from
	cte3
where
	serial_number_count > 1
	
         """




query2 = """
				 -- Returns different customers with duplicated serial numbers only for MD-Medicus Customers (Updated 01.02.2024)

with
	cte1 as (
	          select
	                distinct 
                            -- replace empty serial numbers with null
	                        replace(lo.items_serial_numbers, '[""]', null) as items_serial_numbers,
	                        -- extract device type
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then right(split_part(items_serial_numbers, '-', 3), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(left(split_part(items_serial_numbers, ',', 2), 4), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then substring(split_part(items_serial_numbers, ',', 2), 2, 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
	               		    	when items_serial_numbers like '%GC%' then trim(split_part(items_serial_numbers, '-', 1), '["')
	               		    	when items_serial_numbers like '%SM%' then right(split_part(items_serial_numbers, '-', 1), 2)
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then right(split_part(items_serial_numbers, '-', 1), 2) 
	               		    	when items_serial_numbers like '%SN%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%LS%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%TRC%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '["RB%'then right(split_part(items_serial_numbers, '-', 1), 3)
	               		    	when items_serial_numbers like '["RB%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '%JY%' then substring(items_serial_numbers, 3, 2)
	               		    end
	               		   ) as device_type,
	               		   -- extract serial numbers
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')  
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then trim(substring(items_serial_numbers, 10, 11), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%GC%' then trim(substring(items_serial_numbers, 3, 10), '"\][,')
	               		    	when items_serial_numbers like '%SM%' then trim(substring(items_serial_numbers, 3, 11), '"\,')
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%SN%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%LS%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    	when items_serial_numbers like '%TRC%' then trim(substring(items_serial_numbers, 3, 14), '"\][,')
	               		    	when items_serial_numbers like '["RB%' then trim(substring(items_serial_numbers, 3, 12), '"\][,')
	               		    	when items_serial_numbers like '%JY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    end
	               		    ) as serial_number,
	                        mc."CUSTOMER_ID"  as customer_id,
	                        concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as customer_name, 
	                        lo.redmine_updated_at,
	                        lo.subscription_id as order_subscription_id,
                            lo.redmine_issue_number,
	                        ms."SUBSCRIPTION_ID" as subscription_id,
	                        ms."START"::date as start_date,
	                        -- adjust cancellation date
							(
                             case
                                 when ms."START" > ms."CANCELLATION_DATE" then ms."START" 
                                else ms."CANCELLATION_DATE" 
                              end
                            )::date as end_date,
	                        ms."STATUS" as monsum_status,
	                        oso.status_id as opportunity_status,
	                        ri.status_name as redmine_ticket_status,
	                        ms."ARTICLE_NUMBER" as article_number,
	                        -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as product,
	                        ri.done_ratio as redmine_done_ratio,
	                        lo.emergency_call_center 
			  from oro_mysql.libify__order lo
			  left join redmine_issues ri on lo.redmine_issue_number = ri.id 
			  left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
			  left join oro_mysql.orocrm_sales_opportunity oso on ms."SUBSCRIPTION_ID" = oso.monsum_subscription_id 
              left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
              left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
              /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                * include only MD Medicus customers
                */
              where mc."CUSTOMER_ID" notnull and 
                     ms."STATUS" in ('active', 'trial') and 
                     ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906') and
                     lo.emergency_call_center like '%Medicus%'
	        ),
	cte2 as (
	          select 
	                distinct 
	                        -- items_serial_numbers,
	                        device_type,
	                        serial_number,
	                        customer_id,
	                        customer_name,
	                        redmine_updated_at,
	                        dense_rank() over w as den_rk_redmine,
	                        -- order_subscription_id,
	                        -- redmine_issue_number,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        monsum_status,
	                        opportunity_status,
	                        redmine_ticket_status,
	                        article_number,
	                        product,
	                        redmine_done_ratio,
	                        emergency_call_center 
	          from cte1
	          window w as (partition by customer_id, subscription_id order by redmine_updated_at desc)
	        ),
	cte3 as (
	          select
	                distinct 
	                        device_type,
	                        serial_number,
	                        count(serial_number) over w as serial_number_count,
	                        customer_id,
	                        customer_name,
	                        -- redmine_updated_at,
	                        -- den_rk_redmine,
	                        subscription_id,
	                        start_date,
	                        end_date,
	                        monsum_status,
	                        opportunity_status,
	                        redmine_ticket_status,
	                        -- article_number,
	                        product,
	                        redmine_done_ratio,
	                        emergency_call_center 
	          from cte2
	          where 
	          -- filter latest redmine timestamp by customer id and subscription id
	          den_rk_redmine = 1
	          window w as (partition by serial_number)
	        )
select
	  distinct 
              *
from
	cte3
where
	serial_number_count > 1

         """


query3 = """
         -- returns current MD Medicus customers     -- reporting-db.libify.cloud:5432 --    (Updated 01.02.2024) 

with 
	cte1 as (
	          select 
	                distinct
	                        mc."CUSTOMER_ID" as Kunden_ID,
	                        ms."SUBSCRIPTION_ID" as Abo_ID,
	                        ms."START"::date as Abo_Start,
	                        ms."STATUS" as Abo_Status,
	                        mc."LAST_NAME" as Name,
							mc."FIRST_NAME" as Vorname,
							mc."ADDRESS" as Adresse,
							mc."ZIPCODE" as PLZ,
							mc."CITY" as Ort,
	                        -- fill all missing records of redmine ticket numbers from monsum subscriptions table with records from libify order
	                        coalesce(cast(ms.redmine_ticket_number as text), cast(lo.redmine_issue_number as text)) as redmine_ticket_number,
	                        lri.status as redmine_status, 
	                        lo.progress,
	                        -- lo.items_serial_numbers,
	                        -- extract device type
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then right(split_part(items_serial_numbers, '-', 3), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(left(split_part(items_serial_numbers, ',', 2), 4), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then substring(split_part(items_serial_numbers, ',', 2), 2, 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
	               		    	when items_serial_numbers like '%GC%' then trim(split_part(items_serial_numbers, '-', 1), '["')
	               		    	when items_serial_numbers like '%SM%' then right(split_part(items_serial_numbers, '-', 1), 2)
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then right(split_part(items_serial_numbers, '-', 1), 2) 
	               		    	when items_serial_numbers like '%SN%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%LS%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%TRC%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '["RB%'then right(split_part(items_serial_numbers, '-', 1), 3)
	               		    	when items_serial_numbers like '["RB%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '%JY%' then substring(items_serial_numbers, 3, 2)
	               		    end
	               		   ) as device_type,
	               		   -- extract serial numbers
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')  
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then trim(substring(items_serial_numbers, 10, 11), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%GC%' then trim(substring(items_serial_numbers, 3, 10), '"\][,')
	               		    	when items_serial_numbers like '%SM%' then trim(substring(items_serial_numbers, 3, 11), '"\,')
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%SN%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%LS%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    	when items_serial_numbers like '%TRC%' then trim(substring(items_serial_numbers, 3, 14), '"\][,')
	               		    	when items_serial_numbers like '["RB%' then trim(substring(items_serial_numbers, 3, 12), '"\][,')
	               		    	when items_serial_numbers like '%JY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    end
	               		    ) as serial_number,
	               		    -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as product,
	               		    -- lri.done_ratio,  
	                        lo.emergency_call_center
	          from oro_mysql.libify__order lo
	          left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
	          left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
	          left join oro_mysql.libify__redmine_issue lri on ms.redmine_ticket_number = cast(lri.issue_number as text)
	          /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
              */
	          where mc."CUSTOMER_ID" notnull and
	                ms."STATUS" in ('trial', 'active') and
	                ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906') and
	                lo.progress >= 40 and
	                lo.emergency_call_center like '%Medicus%'
	         ),
	cte2 as (
	          select   
	                distinct
	                        kunden_id,
	                        abo_id,
	                        abo_start,
	                        abo_status,
	                        "name",
	                        vorname,
	                        adresse,
	                        plz,
	                        ort,
	                        redmine_ticket_number,
	                        redmine_status,
	                        progress,
	                        device_type,
	                        serial_number,
	                        product,
	                        emergency_call_center
	          from cte1
	        )
select
	  distinct *
from
	cte2
order by
	abo_start desc
  
         """			 