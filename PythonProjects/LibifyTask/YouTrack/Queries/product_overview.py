query1 = """
				 -- Returns finalized product overview -- oro-mysql-slave.libify.com:30316 (Updated 02.02.2024)

select
	  distinct 
              concat(mc.FIRST_NAME , ' ', mc.LAST_NAME) as customer_name,
	          oa.id as account_id,
	          oso.contact_id,
	          mc.CUSTOMER_ID as customer_id,
	          ms.SUBSCRIPTION_ID as subscription_id,
	          ms.STATUS as subscription_status,
	          -- adjust article names
              /*
                 * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                 * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
              (
                case
                    when ms.ARTICLE_NUMBER in ('1900', '1901') then 'Basic'
                    when ms.ARTICLE_NUMBER = '1902' then 'Mobil'
                    when ms.ARTICLE_NUMBER in ('1904', '1905') then 'Home'
                    when ms.ARTICLE_NUMBER = '1906' then 'Doro'
                  else null
                end
	          ) as product,
	          ms.COUPON_CODE as coupon_code,
	          mi.SUB_TOTAL as invoice_amount,
	          date(max(mi.INVOICE_DATE)) as invoice_date
from oro.monsum__subscription ms 
left join oro.monsum__customer mc on mc.CUSTOMER_ID = ms.CUSTOMER_ID
left join oro.orocrm_sales_opportunity oso on mc.CUSTOMER_ID = oso.monsum_customer_id
left join oro.libify_contact_to_opportunity lcto on lcto.contact_id = oso.contact_id
left join oro.orocrm_account_to_contact oatc on lcto.contact_id = oatc.contact_id
left join oro.orocrm_account oa on oa.id = oatc.account_id
left join oro.monsum__invoice mi on mc.CUSTOMER_ID = mi.customer_id
where mc.CUSTOMER_ID is not null and
     ms.STATUS in ('active', 'trial') and 
     ms.ARTICLE_NUMBER in ('1900', '1901', '1902', '1904', '1905', '1906')
group by
	mc.CUSTOMER_ID
order by mc.CUSTOMER_ID
	
         """