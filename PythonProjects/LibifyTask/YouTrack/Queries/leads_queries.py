query1 = """
         -- Returns All Lead Information (Updated 02.02.2024)

with
    leads_tbl as (
                   select 
                         distinct 
                                 osl.lead_id,
                                 osl."name" as lead_name,
                                 osl.contact_id
                   from oro_mysql.orocrm_sales_lead osl
                 )
select distinct * from leads_tbl      

         """