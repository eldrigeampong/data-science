query1  = """
          -- Returns Migration Data   =====> (Updated 01.02.2024)  

with 
    cte1 as (
             select
                   distinct
                           -- take the latest redmine timestamp, use dense rank as a robust step for by-passing cases of ties in redmine timestamp
                           dense_rank () over (partition by lo.items_serial_numbers order by redmine_updated_at desc) as ranked_redmine_timestamp,
                           lo.items_serial_numbers,
                           -- extract device type
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then right(split_part(items_serial_numbers, '-', 3), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(left(split_part(items_serial_numbers, ',', 2), 4), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then substring(split_part(items_serial_numbers, ',', 2), 2, 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then right(split_part(items_serial_numbers, '-', 1), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then right(trim(split_part(items_serial_numbers, '-', 1), '["'), 3)
	               		    	when items_serial_numbers like '%GC%' then trim(split_part(items_serial_numbers, '-', 1), '["')
	               		    	when items_serial_numbers like '%SM%' then right(split_part(items_serial_numbers, '-', 1), 2)
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then right(split_part(items_serial_numbers, '-', 1), 2) 
	               		    	when items_serial_numbers like '%SN%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%LS%' then substring(items_serial_numbers, 3, 2)
	               		    	when items_serial_numbers like '%TRC%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '["RB%'then right(split_part(items_serial_numbers, '-', 1), 3)
	               		    	when items_serial_numbers like '["RB%' then substring(items_serial_numbers, 3, 3)
	               		    	when items_serial_numbers like '%JY%' then substring(items_serial_numbers, 3, 2)
	               		    end
	               		   ) as device_type,
	               		   -- extract serial numbers
	               		   (
	               		    case
		               		    when (items_serial_numbers like '%RB4%') and (items_serial_numbers like '["Alt%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["ALT%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')  
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Ger%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["",%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["","neu%') then trim(substring(items_serial_numbers, 10, 11), '"\][,')
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["NEU%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
		               		    when (items_serial_numbers like '%GC%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%GC%' then trim(substring(items_serial_numbers, 3, 10), '"\][,')
	               		    	when items_serial_numbers like '%SM%' then trim(substring(items_serial_numbers, 3, 11), '"\,')
	               		    	when (items_serial_numbers like '%SN%') and (items_serial_numbers like '["Neu%') then trim(split_part(items_serial_numbers, ',', 2), '"\][,') 
	               		    	when items_serial_numbers like '%SN%' then trim(substring(items_serial_numbers, 3, 11), '"\][,')
	               		    	when items_serial_numbers like '%LS%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    	when items_serial_numbers like '%TRC%' then trim(substring(items_serial_numbers, 3, 14), '"\][,')
	               		    	when items_serial_numbers like '["RB%' then trim(substring(items_serial_numbers, 3, 12), '"\][,')
	               		    	when items_serial_numbers like '%JY%' then trim(substring(items_serial_numbers, 3, 11), '"\][,') 
	               		    end
	               		    ) as serial_number,
                           lo.subscription_id as order_subscription_id,
                           lo.redmine_issue_number,
                           lo.redmine_updated_at,
                           concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as customer_name,
                           mc."CUSTOMER_ID"  as customer_id,
                           mc."CUSTOMER_NUMBER" as customer_number,
                           ms."SUBSCRIPTION_ID" as subscription_id,
                           date(ms."START") as start_date,
                           -- adjust cancellation datae
                           date(
                                (
                                 case
                                 	when ms."START" > ms."CANCELLATION_DATE" then ms."START" 
                                   else ms."CANCELLATION_DATE" 
                                 end
                                )
                           ) as end_date,
                           ms."STATUS" as subscription_status,
                           -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when ms."ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when ms."ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when ms."ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when ms."ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as product,
	                       lo.emergency_call_center,
	                       lo.progress
             from oro_mysql.libify__order lo 
             left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
             left join oro_mysql.monsum__customer mc on ms."CUSTOMER_ID" = mc."CUSTOMER_ID"
             left join oro_mysql.monsum__article ma on ms."ARTICLE_NUMBER" = ma.article_number
             /*
                * exclude all cases where a customer has no id
                * include only cases of active and trial customers
                * include Basic, Mobil, Home and Doro products
                * filter only 100% progress thus devices en route to customers 
             */
             where mc."CUSTOMER_ID" notnull and
                   ms."STATUS" in ('active', 'trial') and 
                   ms."ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906') and
                   lo.progress = 100
                   
            ),
	cte2 as (
	         select
	               distinct
	                       -- ranked_redmine_timestamp,
	                       -- items_serial_numbers,
	                       customer_name,
	                       customer_id,
	                       customer_number,
	                       subscription_id,
	                       start_date,
	                       end_date,
	                       subscription_status,
	                       product,
	                       device_type,
	                       replace (cte1.serial_number, 'neu: ', '') as serial_number,
	                        -- assign device proprietor names
			               (
			                case
			                    when (d.serial_number like '%gc%' or d.serial_number like '%TRC%') and (d.proprietor_id isnull) then 'Libify'
				                when d.proprietor_id = 'e70a076b-3884-474b-b055-c30622379dfb' then 'Libify'
			                    when d.proprietor_id = 'a169b327-6d9e-4fdc-8d0a-a5e84c1e3a39' then 'Customer'
			                    when (d.serial_number like '%LS%') and (d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = 'bd557bf0-b3a0-4e37-b0e9-2c7d6ebf6de1' then 'Arkea'
			                    when (d.serial_number like '%JY%' and d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = '4fcb1ec0-2485-4a5a-9c1c-a22b50f69e8f' then 'MD Medicus'
			                    when d.proprietor_id = '420955f1-65a1-4521-8534-7309ea7edf50' then 'ÖRK Salzburg'
			                    when d.proprietor_id = '6bb64f85-f577-4fb1-aa70-b8fcd4af08d4' then 'Kineo'
			                    when d.proprietor_id = '23079d33-d170-437e-b683-56888fc435b2' then 'Libify_ExKineo23'
			                  else null
			                 end
			               ) as proprietor_name,
			               -- adjust device type
	         	           (
	         	            case
	         	                when d."type" = 5 then 'GeoX' 
	         	                when d."type" = 6 then 'Mobile' 
	         	                when d."type" = 12 then 'Sim Card'
	         	                when d."type" = 14 then 'RF Module'
	         	                when d."type" = 17 then 'Radio Button'
	         	                when d."type" = 20 then 'Active Cradle'
	         	                when d."type" = 22 then 'Gateway'
	         	                when d."type" = 25 then 'Sensoric'
	         	                when d."type" = 30 then 'Smart Watch'
	         	                when d."type" = 35 then 'Tablet'
	         	              else null
	         	             end
	         	           ) as device_name,
	         	           d.phone_number as device_phone_number,
	         	           emergency_call_center,
	         	           progress,
	         	           redmine_issue_number,
	         	           order_subscription_id
	                       -- redmine_updated_at             
	         from cte1
	         left join devices d on d.serial_number = cte1.serial_number
	         where ranked_redmine_timestamp = 1
	        ),
	cte3 as (
	         select 
	               distinct
	                       customer_name,
	                       customer_id,
	                       -- customer_number,
	                       -- subscription_id,
	                       -- start_date,
	                       -- end_date,
	                       -- subscription_status,
	                       -- product,
	                       trim(device_type) as device_types,
	                       trim(serial_number) as serial_numbers,
	                       -- proprietor_name,
	                       -- device_name,
	                       -- progress,
	                       -- order_subscription_id,
	                       redmine_issue_number,
	                       emergency_call_center,
	                       device_phone_number
	         from cte2
	        )        
select distinct * from cte3 order by customer_id -- returns full list of customers migrated and not yet migrated
-- select distinct * from cte3 where emergency_call_center like '%ASB Dortmund%' order by customer_id -- returns all customers with call center ASB Dortmund (not yet migrated)
-- select distinct * from cte3 where emergency_call_center like '%Medicus%' order by customer_id -- returns all customers with call center MD Medicus (already migrated)

          """