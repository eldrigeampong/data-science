query1 = """
         -- Returns libify customers with product change from 1st July till date -- reporting-db.libify.cloud -- (Updated 01.02.2024)

with
    cte1 as (
              select 
                    distinct
                    		ri.id as redmine_issue_number,        
                    		ri.project_name,
                            ri.status_name,
                            -- ri.subject,
                            ri.done_ratio,
                            lo.redmine_issue_number as lo_redmine_issue_number,
                            -- lo.items_serial_numbers,
                            -- extract device type
                            trim(
                                 (
                                  case 
                             	      when lo.items_serial_numbers like '%GC%' then right(split_part(lo.items_serial_numbers, '-', 1), 3)
                             	      when lo.items_serial_numbers like '%LS%' then substring(lo.items_serial_numbers, 3, 2)
                             	      when lo.items_serial_numbers like '%TRC%' then substring(lo.items_serial_numbers, 3, 3)
                             	      when lo.items_serial_numbers like '%JY%' then substring(lo.items_serial_numbers, 3, 2)
                                    else lo.items_serial_numbers 
                                   end
                                  )
                              ) as device_type,
                              -- extract serial numbers
                              trim(
                                   (
                                    case
                                        when lo.items_serial_numbers like '["",%' then trim(split_part(lo.items_serial_numbers, ',', 2), '"\/')
	                                    when lo.items_serial_numbers like '%GC%' then trim(substring(lo.items_serial_numbers, 3, 10), '"\/')
                                        when lo.items_serial_numbers like '%LS%' then trim(substring(lo.items_serial_numbers, 3, 11), '"\/') 
                                        when lo.items_serial_numbers like '%TRC%' then trim(substring(lo.items_serial_numbers, 3, 14), '"\/') 
                                        when lo.items_serial_numbers like '%JY%' then trim(substring(lo.items_serial_numbers, 3, 11), '"\/')  
                                      else lo.items_serial_numbers 
                                     end
                                    )
                              ) as serial_number,
                              ms."SUBSCRIPTION_ID",
                              ms."START"::date as start_date,
							  -- adjust cancellation date
							  (
                               case
                                   when ms."START" > ms."CANCELLATION_DATE" then ms."START" 
                                 else ms."CANCELLATION_DATE" 
                                end
                              )::date as end_date,
                              ms."STATUS" as abo_status,
                              ms."ARTICLE_NUMBER",
                              ms."CUSTOMER_ID",
                              trim(replace(trim(substring(ms."CANCELLATION_NOTE", '\D+'), ':"\/'), '-', '')) as Stornogrund,
                              ri.updated_on
              from redmine_issues ri
              left join redmine_issue_relations rir on ri.id = rir.to_issue_id
              right join oro_mysql.libify__order lo on rir.issue_id = lo.redmine_issue_number
              left join oro_mysql.monsum__subscription ms on lo.subscription_id = ms.id
              /*
               * Include only project names with the title B2C - Returns 
               * Include only cases with the status name closed
               * Include only cases with subjects containing tauch
               * Include only redmine updated cases beginning 1st July, 2023
              */
              where 
                    project_name = 'B2C - Returns' and 
                    status_name = 'Closed' and 
                    subject like '%tausch%' and
                    ri.updated_on >= '2023-07-01'
             ),
	cte2 as (
	         select 
	               distinct 
	                       concat(mc."FIRST_NAME", ' ', mc."LAST_NAME") as Name_des_Kunden,
	                       mc."CUSTOMER_ID" as Kunden_ID,
	                       -- find old and new customers
	                       /*
	                        * find old and new customers
	                        * if a unique customer has more than one redmine_issue_number, then set customer as new, else set as old 
	                        */
	                       (
	                        case
	                        	when count(redmine_issue_number) over w = 1 then 'Ja'
	                            when count(redmine_issue_number) over w > 1 then 'Nein'
	                          else null
	                        end
	                       ) as ist_Neukunde,
	                       "SUBSCRIPTION_ID" as Abo_ID,
	                       start_date,
	                       end_date,
	                       -- calculate subscription duration
	                       (
	                        case
	                        	when end_date notnull then (end_date - start_date)
	                         else null
	                        end
	                       ) as Laufzeit,
	                       abo_status,
	                       device_type as Gerätetyp,
	                       cte1.serial_number as Seriennummer,
	                       -- assign device proprietor names
			               (
			                case
			                    when (d.serial_number like '%gc%' or d.serial_number like '%TRC%') and (d.proprietor_id isnull) then 'Libify'
				                when d.proprietor_id = 'e70a076b-3884-474b-b055-c30622379dfb' then 'Libify'
			                    when d.proprietor_id = 'a169b327-6d9e-4fdc-8d0a-a5e84c1e3a39' then 'Customer'
			                    when (d.serial_number like '%LS%') and (d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = 'bd557bf0-b3a0-4e37-b0e9-2c7d6ebf6de1' then 'Arkea'
			                    when (d.serial_number like '%JY%' and d.proprietor_id isnull) then 'Arkea'
			                    when d.proprietor_id = '4fcb1ec0-2485-4a5a-9c1c-a22b50f69e8f' then 'MD Medicus'
			                    when d.proprietor_id = '420955f1-65a1-4521-8534-7309ea7edf50' then 'ÖRK Salzburg'
			                    when d.proprietor_id = '6bb64f85-f577-4fb1-aa70-b8fcd4af08d4' then 'Kineo'
			                    when d.proprietor_id = '23079d33-d170-437e-b683-56888fc435b2' then 'Libify_ExKineo23'
			                  else null
			                 end
			               ) as Inhaber,
	                       -- "ARTICLE_NUMBER",
	                       -- adjust article names
                            /*
                             * Link to product names can be found here: https://automatic.fastbill.com/index.php?cmd=35
                             * Link to crm user interface can be found here: https://crm.libify.com/dashboard/view/1?change_dashboard=1
                             */
                            (
                              case
                              	  when "ARTICLE_NUMBER" in ('1900', '1901') then 'Basic'
                              	  when "ARTICLE_NUMBER" = '1902' then 'Mobil'
                              	  when "ARTICLE_NUMBER" in ('1904', '1905') then 'Home'
                              	  when "ARTICLE_NUMBER" = '1906' then 'Doro'
                              	else null
                              end
	                        ) as Produkt,
	                       redmine_issue_number,
	                       count(redmine_issue_number) over w issues_count,
	                       project_name as Name_des_Projekts,
	                       status_name as Name_des_Status,
	                       Stornogrund,
	                       done_ratio as Erledigungsquote,
	                       -- lo_redmine_issue_number,
	                       updated_on::date as Redmine_Zeitstempel
	         from cte1
	         left join oro_mysql.monsum__customer mc on cte1."CUSTOMER_ID" = mc."CUSTOMER_ID"
	         left join oro_mysql.monsum__article ma on cte1."ARTICLE_NUMBER" = ma.article_number
	         left join devices d on cte1.serial_number = d.serial_number
	         /*
                * exclude all cases where a customer has no id
                * include Basic, Mobil, Home and Doro products
             */
             where mc."CUSTOMER_ID" notnull and
                   "ARTICLE_NUMBER" in ('1900', '1901', '1902', '1904', '1905', '1906')
	         window w as (partition by mc."CUSTOMER_ID")
	        ),
	cte3 as (
	         select 
	               distinct 
	                       name_des_kunden,
	                       kunden_id,
	                       ist_neukunde,
	                       abo_id,
	                       start_date,
	                       end_date,
	                       laufzeit,
	                       -- calculate cancellation and churn
	                       (
	                        case
	                        	when laufzeit < 60 then 'cancellation'
	                        	when laufzeit >= 60 then 'churn'
	                         else null 
	                        end
	                       ) as kpi,
	                       abo_status,
	                       gerätetyp,
	                       seriennummer,
	                       produkt,
	                       inhaber,
	                       redmine_issue_number,
	                       issues_count,
	                       name_des_projekts,
	                       name_des_status,
	                       stornogrund,
	                       erledigungsquote,
	                       redmine_zeitstempel
	         from cte2
	        )
-- select distinct * from cte1    
-- select distinct * from cte2
select distinct * from cte3 order by redmine_zeitstempel desc
	        
         """